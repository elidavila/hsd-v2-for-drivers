package com.hopskipdrive.hsd_android_v2.timer_tasks;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;

import org.json.JSONObject;

import java.util.TimerTask;

import javax.inject.Inject;

public class HeartBeatTask extends TimerTask {
    @Inject RequestQueue mRequestQueue;
    JSONObject params = new JSONObject();
    String url;

    public HeartBeatTask(String url) {
        HSDInject.inject(this);
        this.url = url;
    }

    @Override
    public void run() {
        HSDJsonObjectRequest request = new HSDJsonObjectRequest(Request.Method.PUT, url, params, mHeartBeatResponse, mHeartBeatErrorListener);
        mRequestQueue.add(request);
    }

    Response.Listener<JSONObject> mHeartBeatResponse = new Response.Listener<JSONObject>() {
        @Override public void onResponse(JSONObject jsonObject) { }
    };

    Response.ErrorListener mHeartBeatErrorListener = new Response.ErrorListener() {
        @Override public void onErrorResponse(VolleyError volleyError) {
            volleyError.printStackTrace();
        }
    };
}
