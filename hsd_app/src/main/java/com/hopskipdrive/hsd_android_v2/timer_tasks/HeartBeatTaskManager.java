package com.hopskipdrive.hsd_android_v2.timer_tasks;

import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;

import java.util.Timer;

public class HeartBeatTaskManager{
    private static final long HEART_BEAT_INTERVAL_MS = 10 * 1000;
    String url = UrlBuilder.create(UrlBuilder.Endpoint.DEVICE).build();
    Timer timer;

    public void startHeartBeat() {
        timer = new Timer();
        timer.schedule(new HeartBeatTask(url), 0, HEART_BEAT_INTERVAL_MS);
    }

    public void stopHeartBeat() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }
}
