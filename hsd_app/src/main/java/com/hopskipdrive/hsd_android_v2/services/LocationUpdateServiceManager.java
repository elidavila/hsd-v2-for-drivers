package com.hopskipdrive.hsd_android_v2.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;

public class LocationUpdateServiceManager {

    boolean mBound = false;
    private boolean mIsTracking = false;
    LocationUpdateService mLocationUpdateService;
    Context mContext;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdateService.LocalBinder binder = (LocationUpdateService.LocalBinder) service;
            mLocationUpdateService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mLocationUpdateService = null;
            mBound = false;
        }
    };

    public void init(Context context) {
        // Start location update service
        mContext = context;
        Intent intent = new Intent(context, LocationUpdateService.class);
        context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public boolean isBound() {
        return mBound;
    }

    public void startTracking(Ride ride) {
        if (isBound() && RideValidator.isValid(ride) && !mIsTracking) {
            Log.wtf(getClass().getName(), "startTrackingRide");
            mIsTracking = true;
            mContext.startService(new Intent(mContext, LocationUpdateService.class));
            mLocationUpdateService.startTracking(ride);
        }
    }

    public void stopTracking() {
        if (isTracking()) {
            Log.wtf(getClass().getName(), "stopTrackingRide");
            mIsTracking = false;
            mLocationUpdateService.stopTracking();
        }
    }

    public boolean isTracking(){
        return isBound() && mIsTracking;
    }
}
