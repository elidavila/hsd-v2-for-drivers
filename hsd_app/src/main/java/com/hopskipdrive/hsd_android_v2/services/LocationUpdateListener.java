package com.hopskipdrive.hsd_android_v2.services;

import android.location.Location;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.location.LocationListener;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.translators.LocationUpdateTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder.Endpoint;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LocationUpdateListener implements LocationListener {
    private final Ride mRide;
    private final Map<Long, Location> mPendingUpdates = new ConcurrentHashMap<>();
    private boolean mUpdateInProgress;

    @Inject RequestQueue mRequestQueue;

    private Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            Log.wtf("Tracking error", volleyError.toString());
            mUpdateInProgress = false;
        }
    };

    public LocationUpdateListener(Ride ride) {
        HSDInject.inject(this);
        mRide = ride;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(getClass().getSimpleName(),
            String.format("onLocationChanged [%d] lat:%.2f, lng:%.2f",
                location.getTime(),
                location.getLatitude(),
                location.getLongitude()));

        if (RideValidator.isValid(mRide)) {
            mPendingUpdates.put(location.getTime(), location);
            postPendingUpdates();
        }
    }

    private synchronized void postPendingUpdates() {
        if (RideValidator.isValid(mRide) && !mPendingUpdates.isEmpty() && !mUpdateInProgress) {
            mUpdateInProgress = true;
            final Map<Long, Location> pendingUpdatesToSend = new HashMap<>(mPendingUpdates);
            JSONArray postData = LocationUpdateTranslator.getUpdatesArray(mRide, pendingUpdatesToSend.values());
            Map<String, String> queryParams = LocationUpdateTranslator.getUpdatesArrayQueryParams(mRide);
            String url = UrlBuilder.create(Endpoint.POSITION).withParams(queryParams).build();

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    if (JSONHelper.getInt(jsonObject, "ok") == 1) {
                        for (Long key : pendingUpdatesToSend.keySet()) {
                            mPendingUpdates.remove(key);
                        }
                        Log.wtf(getClass().getName(), "location update successful!");
                    }
                    mUpdateInProgress = false;
                }
            };

            HSDJsonObjectRequest request = new HSDJsonObjectRequest(Request.Method.POST, url, postData, responseListener, mErrorListener);
            mRequestQueue.add(request);
        }
    }
}
