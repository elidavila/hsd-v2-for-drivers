package com.hopskipdrive.hsd_android_v2.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.hopskipdrive.hsd_android_v2.exception.GPServicesUnavailableException;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.util.AndroidUtils;

public class LocationUpdateService extends Service
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final long UPDATE_INTERVAL_MS = 10 * 1000;

    IBinder mBinder = new LocalBinder();
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LocationUpdateListener mListener;

    public class LocalBinder extends Binder {
        public LocationUpdateService getService() {
            return LocationUpdateService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!AndroidUtils.isGooglePlayServicesAvailable(this)) {
            stopSelf();
            Crashlytics.logException(new GPServicesUnavailableException("Google Play Services unavailable on this client"));
        }

        // Set up Google API Client and connect
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addOnConnectionFailedListener(this)
            .addConnectionCallbacks(this)
            .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // TODO: Remove location request, disconnect from Google API Client
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(getClass().getSimpleName(), "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(getClass().getSimpleName(), "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(getClass().getSimpleName(), "onConnectionFailed");
    }

    public void startTracking(Ride ride) {
        Log.wtf(getClass().getSimpleName(), "startTracking");

        if (mGoogleApiClient.isConnected()) {
            Log.wtf(getClass().getSimpleName(), "Google API client is connected, really startTracking");
            mListener = new LocationUpdateListener(ride);
            mLocationRequest = LocationRequest.create()
                .setInterval(UPDATE_INTERVAL_MS)
                .setFastestInterval(UPDATE_INTERVAL_MS)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mListener);

            // TODO: We should send a persistent notification to the status bar
        }
    }

    public void stopTracking() {
        Log.wtf(getClass().getSimpleName(), "stopTracking");

        if (mGoogleApiClient != null && mListener != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mListener);
        }

        // TODO: We should clear the persistent notification to the status bar
    }
}
