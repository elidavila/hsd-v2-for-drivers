package com.hopskipdrive.hsd_android_v2.net;

import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class HSDJsonObjectRequest extends JsonRequest<JSONObject> {
    @Inject Context mContext;

    public HSDJsonObjectRequest(int method, String url, JSONObject jsonRequest,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
        super(method, url, jsonRequest.toString(), listener, errorListener);
        HSDInject.inject(this);
    }

    public HSDJsonObjectRequest(int method, String url, JSONArray jsonRequest,
                                Response.Listener<JSONObject> listener,
                                Response.ErrorListener errorListener) {
        super(method, url, jsonRequest.toString(), listener, errorListener);
        HSDInject.inject(this);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        String cookie = HelperFunctions.getCookie(mContext);
        headers.put("Cookie", cookie);
        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String je = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(je), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException var3) {
            return Response.error(new ParseError(var3));
        } catch (JSONException var4) {
            return Response.error(new ParseError(var4));
        }
    }
}
