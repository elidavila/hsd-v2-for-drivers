package com.hopskipdrive.hsd_android_v2.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.activities.DriveDetailsActivity;
import com.hopskipdrive.hsd_android_v2.activities.OMWDropOffActivity;
import com.hopskipdrive.hsd_android_v2.activities.OnMyWayActivity;
import com.hopskipdrive.hsd_android_v2.activities.PickUpActivity;
import com.hopskipdrive.hsd_android_v2.activities.RideCompletedActivity;
import com.hopskipdrive.hsd_android_v2.adapters.RidesAdapter;
import com.hopskipdrive.hsd_android_v2.adapters.RidesPagerAdapter;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.exception.HSDException;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.RidesValidator;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;

import javax.inject.Inject;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RidesListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_LIST_TYPE = "LIST_TYPE";
    private static final String INTENT_KEY_RIDE_ID = "Ride.Id";
    private static final Comparator<? super Ride> RIDES_COMPLETED_COMPARATOR = new Comparator<Ride>() {
        @Override
        public int compare(Ride lhs, Ride rhs) {
            // Compare in reverse date order
            return rhs.getPickUpDate().compareTo(lhs.getPickUpDate());
        }
    };

    private RidesPagerAdapter.RidesListType mType;

    @InjectView(R.id.swipe) SwipeRefreshLayout mSwipe;
    @InjectView(R.id.recycler_view) RecyclerView mRecyclerView;
    @InjectView(R.id.no_rides_layout) LinearLayout mNoRidesView;
    @InjectView(R.id.update_availability_button) CardView updateAvailabilityButton;

    boolean isDownloading = false;
    RidesAdapter mRidesAdapter;
    UrlBuilder.Endpoint mEndpoint;

    @Inject RequestQueue mRequestQueue;
    @Inject HSDSession mSession;
    @Inject LocationUpdateServiceManager mLocationUpdateManager;

    private Response.Listener<String> mRidesResponse = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mNoRidesView.setVisibility(View.GONE);
            isDownloading = false;
            mSwipe.setRefreshing(false);
            List<Ride> rides = RidesTranslator.getRidesList(response);
            if (RidesValidator.isValid(rides)) {
                if(mEndpoint.equals(UrlBuilder.Endpoint.RIDES)) {
                    mSession.setUpcomingRides(rides);
                }
                else if(mEndpoint.equals(UrlBuilder.Endpoint.RIDES_COMPLETED)) {
                    Collections.sort(rides, RIDES_COMPLETED_COMPARATOR);
                    mSession.setCompletedRides(rides);
                }
                mRidesAdapter.bind(rides);
            }else if(RidesValidator.isEmpty(rides)){
                mRecyclerView.setVisibility(View.GONE);
                mNoRidesView.setVisibility(View.VISIBLE);
                updateAvailabilityButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(getResources().getString(R.string.server_url_string) + "/schedule"));
                        startActivity(intent);
                    }
                });
            }
        }
    };

    private Response.ErrorListener mRidesErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            isDownloading = false;
            mSwipe.setRefreshing(false);

            // TODO: Implement error handling
        }
    };

    private RidesAdapter.OnRideClickedListener mRideClickedListener = new RidesAdapter.OnRideClickedListener() {
        @Override
        public void onRideClicked(Ride ride) {
            Class<?> destinationActivity = getNextActivity(ride);
            if (destinationActivity != null) {
                mSession.setCurrentRide(ride);
                Intent intent = new Intent(getActivity(), destinationActivity);
                intent.putExtra(INTENT_KEY_RIDE_ID, ride.get_id());
                startActivity(intent);
            }
            else {
                HelperFunctions.showLongToast("Unable to open this ride.  The error has been reported.", getActivity());
                // If we are unable to determine where to
                Crashlytics.logException(
                    new HSDException("Unable to display ride with id %s, status %s and checkedIn %b",
                        ride.get_id(),
                        ride.getStatus(),
                        ride.isCheckedIn()));
            }
        }
    };

    private Class<?> getNextActivity(Ride ride) {
        Class<?> nextActivity = null;
        switch (ride.getStatus()) {
            case Ride.STATUS_MATCHED:
                if (ride.isCheckedIn()) {
                    nextActivity = OnMyWayActivity.class;
                    if(!mLocationUpdateManager.isTracking())
                        mLocationUpdateManager.startTracking(ride);
                }
                else {
                    nextActivity = DriveDetailsActivity.class;
                }
                break;
            case Ride.STATUS_ARRIVED:
                nextActivity = PickUpActivity.class;
                if(!mLocationUpdateManager.isTracking())
                    mLocationUpdateManager.startTracking(ride);
                break;
            case Ride.STATUS_IN_PROGRESS:
                nextActivity = OMWDropOffActivity.class;
                if(!mLocationUpdateManager.isTracking())
                    mLocationUpdateManager.startTracking(ride);
                break;
            case Ride.STATUS_DELIVERED:
                nextActivity = RideCompletedActivity.class;
                break;
        }
        return nextActivity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HSDInject.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rides_list, container, false);
        initControls(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initRidesList();
    }

    private void initControls(View view) {
        ButterKnife.inject(this, view);
        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeColors(
            getResources().getColor(R.color.hsd_blue),
            getResources().getColor(R.color.orange_color)
        );
    }

    @Override
    public void onRefresh() {
        if (!isDownloading) {
            requestRidesList();
        }
    }

    private void initRidesList() {
        int ordinal = getArguments().getInt(ARG_LIST_TYPE);
        mType = RidesPagerAdapter.RidesListType.values()[ordinal];
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRidesAdapter = new RidesAdapter(mRideClickedListener);
        mRecyclerView.setAdapter(mRidesAdapter);

        switch (mType) {
            case COMPLETED:
                mRecyclerView.setEnabled(false);
                mEndpoint = UrlBuilder.Endpoint.RIDES_COMPLETED;
                break;
            case UPCOMING:
                mRecyclerView.setEnabled(true);
                mEndpoint = UrlBuilder.Endpoint.RIDES;
                break;
        }

        requestRidesList();
    }

    private void requestRidesList() {
        // Set flags/UI
        isDownloading = true;
        mSwipe.setRefreshing(true);

        // Initialize endpoint builder
        // TODO: Have to add ability to include params map in UrlBuilder
        String url = UrlBuilder.create(mEndpoint).build() + "?limit=20";

        // Request
        StringRequest request = new StringRequest(Request.Method.GET, url, mRidesResponse, mRidesErrorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> headers = new HashMap<>();
                String cookie = HelperFunctions.getCookie(getActivity());
                headers.put("Cookie", cookie);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    public static RidesListFragment newInstance(RidesPagerAdapter.RidesListType type) {
        RidesListFragment fragment = new RidesListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LIST_TYPE, type.ordinal());
        fragment.setArguments(args);
        return fragment;
    }
}
