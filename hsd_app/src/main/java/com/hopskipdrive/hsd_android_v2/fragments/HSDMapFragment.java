package com.hopskipdrive.hsd_android_v2.fragments;

import com.google.android.gms.maps.SupportMapFragment;

public class HSDMapFragment extends SupportMapFragment {

    public interface OnResumeListener {
        void onResume(HSDMapFragment fragment);
    }

    private OnResumeListener onResumeListener;
    public void setOnResumeListener(OnResumeListener listener) {
        onResumeListener = listener;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeListener != null) {
            onResumeListener.onResume(this);
        }
    }
}
