package com.hopskipdrive.hsd_android_v2.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Eli on 5/29/2015.
 * This dialog box will prompt the user if he or she
 * would like to call support. If they press Call, then it should
 * automatically make the call to our customer support team.
 */
public class CallSupportDialog extends Dialog {
    @InjectView(R.id.call_button) TextView callButton;
    @InjectView(R.id.cancel_call_button) TextView cancelCallButton;
    Activity activity;

    public CallSupportDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_support);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.cancel_call_button)
    protected void onCancelCallClick() {
        dismiss();
    }


    @OnClick(R.id.call_button)
    protected void onCallClick() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:8444677547"));
        activity.startActivity(intent);
        dismiss();
    }
}