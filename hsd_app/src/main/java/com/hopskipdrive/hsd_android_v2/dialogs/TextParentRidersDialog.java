package com.hopskipdrive.hsd_android_v2.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hopskipdrive.hsd_android_v2.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TextParentRidersDialog extends Dialog{
    @InjectView(R.id.texting_list) ListView contactListView;
    Activity activity;
    LinkedHashMap<String, String> phoneNumbers;

    public TextParentRidersDialog(Activity activity, LinkedHashMap<String, String> phoneNumbers) {
        super(activity);
        this.activity = activity;
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_text_parent_riders);
        ButterKnife.inject(this);

        //Set names
        List<String> names = new ArrayList<>();
        names.addAll(phoneNumbers.keySet());

        //Add names to list view
        ArrayAdapter adapter = new ArrayAdapter<>(activity, R.layout.text_contact_row, names);
        contactListView.setAdapter(adapter);

        //Set on what happens when they click on a name
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                openSMSWithRecipient(new ArrayList<>(phoneNumbers.values()).get(position));
            }
        });
    }

    private void openSMSWithRecipient(String number) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", number);
        activity.startActivity(smsIntent);
    }

    @OnClick(R.id.cancel_texting)
    protected void onCancelClick() {
        dismiss();
    }
}
