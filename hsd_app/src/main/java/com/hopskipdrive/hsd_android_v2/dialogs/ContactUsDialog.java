package com.hopskipdrive.hsd_android_v2.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.hopskipdrive.hsd_android_v2.R;

/**
 * Created by Eli on 5/29/2015. This dialog box
 * will display 3 buttons that can email us,
 * call us, or cancel this action.
 */
public class ContactUsDialog extends Dialog{
    Activity activity;
    @InjectView(R.id.contactEmailUs) TextView emailUsButton;
    @InjectView(R.id.contactCallUs) TextView callUsButton;
    @InjectView(R.id.contactCancel) TextView cancelButton;


    public ContactUsDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_contact_us);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.contactEmailUs)
    protected void onEmailUsClick() {
        Uri uri = Uri.parse("mailto:contact@hopskipdrive.com");
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        activity.startActivity(emailIntent);
        dismiss();
    }

    @OnClick(R.id.contactCallUs)
    protected void onCallUsClick() {
        CallSupportDialog callSupportDialog = new CallSupportDialog(activity);
        callSupportDialog.show();
        dismiss();
    }

    @OnClick(R.id.contactCancel)
    protected void onCancelClick() {
        dismiss();
    }

}