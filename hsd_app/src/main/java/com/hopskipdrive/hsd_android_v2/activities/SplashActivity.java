package com.hopskipdrive.hsd_android_v2.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.models.validators.UserValidator;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.SessionUtils;

import javax.inject.Inject;

/**
 * Our splash screen to display our logo while the app loads up.
 */
public class SplashActivity extends Activity {
    @Inject HSDSession mSession;
    int loginType;
    SessionUtils sessionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        HSDInject.inject(this);
        startApp();
    }

    private void startApp() {
        Thread splashThread =  new Thread(){
            @Override public void run(){
                try{
                    sleep(1000);    //Allow our logo to show atleast 1 second
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    loginType = HelperFunctions.getIntPreference("loginType", SplashActivity.this);
                    if(loginType == 1) {
                        //Normal Login
                        if(!UserValidator.isValid(mSession.getUser()) && !SessionUtils.isCaching) {
                            sessionUtils = new SessionUtils(getApplicationContext());
                            sessionUtils.cacheUserSession();
                        }
                        goToRidesActivity();
                    }else {
                        goToLoginActivity();
                    }
                }
            } };
        splashThread.start();
    }

    private void goToLoginActivity() {
        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    private void goToRidesActivity() {
        Intent intent = new Intent(SplashActivity.this, RidesActivity.class);
        startActivity(intent);
        finish();
    }
}