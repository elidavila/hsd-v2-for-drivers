package com.hopskipdrive.hsd_android_v2.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.dialogs.TextParentRidersDialog;
import com.hopskipdrive.hsd_android_v2.fragments.HSDMapFragment;
import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.translators.AddressTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;
import com.hopskipdrive.hsd_android_v2.timer_tasks.HeartBeatTaskManager;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import com.hopskipdrive.hsd_android_v2.util.TransparentProgressDialog;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder.Endpoint;
import com.hopskipdrive.hsd_android_v2.util.ViewHelper;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import org.json.JSONObject;

import javax.inject.Inject;

import java.util.List;

public class OnMyWayActivity extends AppCompatActivity {
    @InjectView(R.id.OMWToolbar) Toolbar mToolbar;
    @InjectView(R.id.OMWPassengerList) LinearLayout mPassengerLayout;

    @InjectView(R.id.ride_pickup_time) TextView ridePickupTimeView;
    @InjectView(R.id.ride_pickup_address_name) TextView ridePickupAddressNameView;
    @InjectView(R.id.ride_pickup_date) TextView ridePickupDateView;
    @InjectView(R.id.ride_pickup_address) TextView ridePickupAddressView;
    @InjectView(R.id.ride_dropoff_time) TextView rideDropoffTimeView;
    @InjectView(R.id.ride_dropoff_address_name) TextView rideDropoffAddressNameView;
    @InjectView(R.id.ride_dropoff_address) TextView rideDropoffAddressView;

    @InjectView(R.id.omw_pickup_notes) TextView pickupNotesView;
    @InjectView(R.id.omw_dropoff_notes) TextView dropoffNotesView;
    @InjectView(R.id.omw_text_parent_rider) TextView textParentRiderView;
    @InjectView(R.id.omw_title) TextView mTitleView;
    @InjectView(R.id.omw_navigation_button) ImageView mNavigationView;

    @Inject HSDSession mSession;
    @Inject RequestQueue mRequestQueue;
    @Inject LocationUpdateServiceManager mLocationUpdateManager;
    @Inject HeartBeatTaskManager mHeartBeatTaskManager;

    HSDMapFragment mMapFragment;
    TransparentProgressDialog loadingLock;
    Ride drive;
    String navigationPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_my_way);
        initialize();
        initMapView();
        setOMWToPickupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!RideValidator.isValid(drive)){
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void initialize() {
        HSDInject.inject(this);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        loadingLock = new TransparentProgressDialog(this);
        loadingLock.setMessage("Please Wait...");
        navigationPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference), this);
        drive = mSession.getCurrentRide();
    }

    private void initMapView() {
        //Set their preferred navigation app image
        if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
            mNavigationView.setImageResource(R.mipmap.waze_icon_transparent);
        else
            mNavigationView.setImageResource(R.mipmap.google_map_icon_transparent);

        //Set map
        mMapFragment = new HSDMapFragment();
        mMapFragment.setOnResumeListener(new HSDMapFragment.OnResumeListener() {
            @Override
            public void onResume(HSDMapFragment fragment) {
                if (fragment != null && fragment.getMap() != null) {
                    fragment.getMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            initMapLocation();
                        }
                    });
                }
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.omw_map_container, mMapFragment).commit();
    }

    private void initMapLocation() {
        if (mMapFragment != null) {
            GoogleMap map = mMapFragment.getMap();
            if (map != null && RideValidator.isValid(drive) &&
                    AddressValidator.isValid(drive.getSaddr()) && AddressValidator.isValid(drive.getDaddr()))
            {
                int dimension = Math.min(mMapFragment.getView().getWidth(), mMapFragment.getView().getHeight());
                int padding = (int) (dimension * 0.3);

                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.getUiSettings().setZoomControlsEnabled(true);

                // Set Map boundaries
                LatLngBounds.Builder builder = LatLngBounds.builder();
                builder.include(new LatLng(drive.getSaddr().getLat(), drive.getSaddr().getLng()));
                builder.include(new LatLng(drive.getDaddr().getLat(), drive.getDaddr().getLng()));
                LatLngBounds bounds = builder.build();
                CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.moveCamera(update);

                // Add map markers
                map.addMarker(getMarker(drive.getSaddr(), R.color.orange_color, "Pick Up")).showInfoWindow();
                map.addMarker(getMarker(drive.getDaddr(), R.color.hsd_blue, "Drop Off"));
            }else{
                HelperFunctions.restartApp(this);
                finish();            }
        }
    }

    private MarkerOptions getMarker(Address address, @ColorRes int colorResid, String title) {
        float[] hsv = new float[3];
        Color.colorToHSV(getResources().getColor(colorResid), hsv);
        float hue = hsv[0];
        return new MarkerOptions()
                .position(new LatLng(address.getLat(), address.getLng()))
                .icon(BitmapDescriptorFactory.defaultMarker(hue))
                .title(title);
    }

    @OnClick(R.id.omw_navigation_button)
    protected void onOMWNavigationButtonClicked(){
        if(RideValidator.isValid(drive) && AddressValidator.isValid(drive.getSaddr())) {
            if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
                ViewUtils.openAppForDirections(this, false, String.valueOf(drive.getSaddr().getLat()), String.valueOf(drive.getSaddr().getLng()));
            else
                ViewUtils.openAppForDirections(this,true, String.valueOf(drive.getSaddr().getLat()), String.valueOf(drive.getSaddr().getLng()));
        }
    }

    private void setOMWToPickupViews() {
        if(RideValidator.isValid(drive)){
            mLocationUpdateManager.startTracking(drive);
            mHeartBeatTaskManager.startHeartBeat();
            mTitleView.setText(drive.getName());
            setPickupDropoffCardView(drive);
            setPassengerInfoCardView(drive);
            ViewHelper.setText(pickupNotesView, drive.getPickUpNotes(), getString(R.string.pickup_notes_none));
            ViewHelper.setText(dropoffNotesView, drive.getDropOffNotes(), getString(R.string.drop_off_notes_none));
        }
    }

    //Sets the pick up and drop off card view with ride data
    private void setPickupDropoffCardView(Ride drive)
    {
        //Pick Up time
        if(drive.getPickup() != null) {
            String pickupTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getPickup());
            ridePickupTimeView.setText(pickupTime);
        }

        //Pick Up name & address
        if(AddressValidator.isValid(drive.getSaddr())){
            ridePickupAddressNameView.setText(drive.getSaddr().getName());
            ridePickupAddressView.setText(AddressTranslator.getDisplayText(drive.getSaddr()));
        }

        //Pick Up date
        String[] driveTimes;
        if(drive.getPickUpDate() != null) {
            driveTimes = HelperFunctions.getRegularTime(drive.getPickUpDate().toString());
            ridePickupDateView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
        }

        //Drop off time
        if(drive.getDropoff() != null) {
            String dropoffTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getDropoff());
            rideDropoffTimeView.setText(dropoffTime);
        }

        //Drop off name & address
        if(AddressValidator.isValid(drive.getDaddr())){
            rideDropoffAddressNameView.setText(drive.getDaddr().getName());
            rideDropoffAddressView.setText(AddressTranslator.getDisplayText(drive.getDaddr()));
        }
    }

    //Sets the data for the passenger card view
    private void setPassengerInfoCardView(Ride drive) {
        List<Rider> riders = drive.getRiders();
        if (RiderValidator.isValid(riders)) {
            if(riders.size() == 1)
                textParentRiderView.setText("Text Parent & Rider");
            else
                textParentRiderView.setText("Text Parent & Riders");

            for (int i = 0; i < riders.size(); i++)
                ViewUtils.addShortPassengerList(OnMyWayActivity.this, mPassengerLayout, riders.get(i), i + 1);
        }
    }

    @OnClick(R.id.arrived_button)
    protected void onArrivedClick() {
        if(RideValidator.isValid(drive)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(OnMyWayActivity.this);
            builder.setMessage("Are you sure you have arrived at:" + "\n\n" +
                    drive.getSaddr().getAddr1() + "\n" +
                    drive.getSaddr().getCity() + ", " + drive.getSaddr().getState() + " " + drive.getSaddr().getZip())
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    requestArrived();
                                }
                            })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }else{
            HelperFunctions.showLongToast("Something went wrong, please try again.", this);
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void requestArrived() {
        String url = UrlBuilder.create(Endpoint.RIDE).withArgs(drive.get_id()).build();
        JSONObject params = RidesTranslator.getRideStatusParams(Ride.Status.ARRIVED);
        HSDJsonObjectRequest request = new HSDJsonObjectRequest(
                Request.Method.PUT, url, params, mArrivedResponse, mArrivedErrorListener);

        if (loadingLock != null)
            loadingLock.show();

        mRequestQueue.add(request);
    }

    Response.Listener<JSONObject> mArrivedResponse = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            ViewUtils.dismiss(loadingLock);

            if(JSONHelper.containsKey(jsonObject, "ride"))
            {
                JSONObject rideObject = JSONHelper.getJSONObject(jsonObject, "ride");
                drive = RidesTranslator.getRide(rideObject);
                if (RideValidator.isValid(drive)) {
                    mSession.setCurrentRide(drive);
                    Intent i = new Intent(OnMyWayActivity.this, PickUpActivity.class);
                    startActivity(i);
                    finish();
                }
            }else{
                //TODO handle what happens when we get an invalid response
            }
        }
    };

    Response.ErrorListener mArrivedErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            ViewUtils.dismiss(loadingLock);
            volleyError.printStackTrace();
            HelperFunctions.showLongToast("We couldn't process your arrived request. Please call us to assist you.", OnMyWayActivity.this);
            CallSupportDialog callSupportDialog = new CallSupportDialog(OnMyWayActivity.this);
            callSupportDialog.show();
            //TODO add crashlytics
        }
    };

    @OnClick(R.id.textParentKidsButton)
    protected void onTextParentKidsClick() {
        if(RideValidator.isValid(drive)){
            TextParentRidersDialog dialog = new TextParentRidersDialog(OnMyWayActivity.this, drive.getPhone_numbers());
            dialog.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_on_my_way, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.on_my_way_cancel_ride:
                CallSupportDialog callSupportDialog = new CallSupportDialog(this);
                callSupportDialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
