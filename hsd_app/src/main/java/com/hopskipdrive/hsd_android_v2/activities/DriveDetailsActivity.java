package com.hopskipdrive.hsd_android_v2.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.fragments.HSDMapFragment;
import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.User;
import com.hopskipdrive.hsd_android_v2.models.translators.AddressTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RidesValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.util.AndroidUtils;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import com.hopskipdrive.hsd_android_v2.util.TransparentProgressDialog;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder.Endpoint;
import com.hopskipdrive.hsd_android_v2.util.ViewHelper;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import org.json.JSONObject;

import javax.inject.Inject;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class DriveDetailsActivity extends AppCompatActivity {
    private static final String INTENT_KEY_RIDE_ID = "Ride.Id";
    public static final String CURRENT_RIDE_ID_KEY = "Current.Ride.Id";

    @InjectView(R.id.DriveDetailsToolbar) Toolbar mToolbar;
    @InjectView(R.id.DriveDetailsPassengerList) LinearLayout mPassengerLayout;
    @InjectView(R.id.ride_pickup_time) TextView ridePickupTimeView;
    @InjectView(R.id.ride_pickup_address_name) TextView ridePickupAddressNameView;
    @InjectView(R.id.ride_pickup_date) TextView ridePickupDateView;
    @InjectView(R.id.ride_pickup_address) TextView ridePickupAddressView;
    @InjectView(R.id.ride_dropoff_time) TextView rideDropoffTimeView;
    @InjectView(R.id.ride_dropoff_address_name) TextView rideDropoffAddressNameView;
    @InjectView(R.id.ride_dropoff_address) TextView rideDropoffAddressView;
    @InjectView(R.id.drive_details_pickup_notes) TextView pickupNotesView;
    @InjectView(R.id.drive_details_dropoff_notes) TextView dropoffNotesView;
    @InjectView(R.id.drive_details_title) TextView mTitleView;
    @InjectView(R.id.drive_details_navigation_button) ImageView mNavigationView;
    @InjectView(R.id.checkInButton) CardView checkinButton;

    HSDMapFragment mMapFragment;

    @Inject RequestQueue mRequestQueue;
    @Inject HSDSession mSession;

    User user;
    Ride drive;
    TransparentProgressDialog loadingLock;
    String navigationPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_details);
        initialize();
        initMapView();
        setDriveDetailViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!RideValidator.isValid(drive)){
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void initialize() {
        HSDInject.inject(this);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        loadingLock = new TransparentProgressDialog(this);
        loadingLock.setMessage("Please Wait...");
        navigationPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference), this);
        drive = getRideById(getBundledRideId());
    }

    private void initMapView() {
        //Set their preferred navigation app image
        if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
            mNavigationView.setImageResource(R.mipmap.waze_icon_transparent);
        else
            mNavigationView.setImageResource(R.mipmap.google_map_icon_transparent);

        //Set map
        mMapFragment = new HSDMapFragment();
        mMapFragment.setOnResumeListener(new HSDMapFragment.OnResumeListener() {
            @Override
            public void onResume(HSDMapFragment fragment) {
                if (fragment != null && fragment.getMap() != null) {
                    fragment.getMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            initMapLocation();
                        }
                    });
                }
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.map_container, mMapFragment).commit();
    }

    private void initMapLocation() {
        if (mMapFragment != null) {
            GoogleMap map = mMapFragment.getMap();
            if (map != null && RideValidator.isValid(drive) &&
                    AddressValidator.isValid(drive.getSaddr()) && AddressValidator.isValid(drive.getDaddr()))
            {
                int dimension = Math.min(mMapFragment.getView().getWidth(), mMapFragment.getView().getHeight());
                int padding = (int) (dimension * 0.3);

                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.getUiSettings().setZoomControlsEnabled(true);

                // Set Map boundaries
                LatLngBounds.Builder builder = LatLngBounds.builder();
                builder.include(new LatLng(drive.getSaddr().getLat(), drive.getSaddr().getLng()));
                builder.include(new LatLng(drive.getDaddr().getLat(), drive.getDaddr().getLng()));
                LatLngBounds bounds = builder.build();
                CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.moveCamera(update);

                // Add map markers
                map.addMarker(getMarker(drive.getSaddr(), R.color.orange_color, "Pick Up")).showInfoWindow();
                map.addMarker(getMarker(drive.getDaddr(), R.color.hsd_blue, "Drop Off"));
            }else{
                HelperFunctions.restartApp(this);
                finish();
            }
        }
    }

    private MarkerOptions getMarker(Address address, @ColorRes int colorResid, String title) {
        float[] hsv = new float[3];
        Color.colorToHSV(getResources().getColor(colorResid), hsv);
        float hue = hsv[0];
        return new MarkerOptions()
                .position(new LatLng(address.getLat(), address.getLng()))
                .icon(BitmapDescriptorFactory.defaultMarker(hue))
                .title(title);
    }

    //Sets all of the ride data in this view
    private void setDriveDetailViews() {
        if(RideValidator.isValid(drive)){
            mTitleView.setText(drive.getName());
            setCheckinButton(drive);
            setPickupDropoffCardView(drive);
            setPassengerInfoCardView(drive);
            ViewHelper.setText(pickupNotesView, drive.getPickUpNotes(), getString(R.string.pickup_notes_none));
            ViewHelper.setText(dropoffNotesView, drive.getDropOffNotes(), getString(R.string.drop_off_notes_none));
        }
    }

    private void setCheckinButton(Ride drive) {
        long pickUpTimeStamp = drive.getPickup_ts();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault());
        long[] timeAwayArray = HelperFunctions.getTimeAwayFromPickup(Math.abs(pickUpTimeStamp - cal.getTimeInMillis()));
        if ((timeAwayArray[0] == 0 && timeAwayArray[1] <= 2) || cal.getTimeInMillis() > pickUpTimeStamp){
            checkinButton.setEnabled(true);
            checkinButton.setCardBackgroundColor(getResources().getColor(R.color.hsd_blue));
        }else{
            checkinButton.setEnabled(false);
            checkinButton.setCardBackgroundColor(getResources().getColor(R.color.light_gray_disabled));
        }
    }

    //Gets the ride from our cached ride list
    private Ride getRideById(String rideId) {
        List<Ride> drives = mSession.getUpcomingRides();
        if (RidesValidator.isValid(drives))
            for (int i = 0; i < drives.size(); i++) {
                Ride drive = drives.get(i);
                if (drive.get_id().equals(rideId))
                    return drive;   //This is our drive we are looking for
            }
        return null;
    }

    @OnClick(R.id.drive_details_navigation_button)
    protected void onNavigationButtonClicked(){
        if(RideValidator.isValid(drive) && AddressValidator.isValid(drive.getSaddr())) {
            if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
                ViewUtils.openAppForDirections(this, false, String.valueOf(drive.getSaddr().getLat()), String.valueOf(drive.getSaddr().getLng()));
            else
                ViewUtils.openAppForDirections(this,true, String.valueOf(drive.getSaddr().getLat()), String.valueOf(drive.getSaddr().getLng()));
        }
    }

    //Sets the pick up and drop off card view with ride data
    private void setPickupDropoffCardView(Ride drive)
    {
        //Pick Up time
        if(drive.getPickup() != null) {
            String pickupTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getPickup());
            ridePickupTimeView.setText(pickupTime);
        }

        //Pick Up name & address
        if(AddressValidator.isValid(drive.getSaddr())){
            ridePickupAddressNameView.setText(drive.getSaddr().getName());
            ridePickupAddressView.setText(AddressTranslator.getDisplayText(drive.getSaddr()));
        }

        //Pick Up date
        String[] driveTimes;
        if(drive.getPickUpDate() != null) {
            driveTimes = HelperFunctions.getRegularTime(drive.getPickUpDate().toString());
            ridePickupDateView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
        }

        //Drop off time
        if(drive.getDropoff() != null) {
            String dropoffTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getDropoff());
            rideDropoffTimeView.setText(dropoffTime);
        }

        //Drop off name & address
        if(AddressValidator.isValid(drive.getDaddr())){
            rideDropoffAddressNameView.setText(drive.getDaddr().getName());
            rideDropoffAddressView.setText(AddressTranslator.getDisplayText(drive.getDaddr()));
        }

    }

    //Sets the data for the passenger card view
    private void setPassengerInfoCardView(Ride drive) {
        List<Rider> riders = drive.getRiders();
        if (RiderValidator.isValid(riders))
            for (int i = 0; i < riders.size(); i++)
                ViewUtils.addShortPassengerList(DriveDetailsActivity.this, mPassengerLayout, riders.get(i), i+1);
    }

    @OnClick(R.id.checkInButton)
    protected void onCheckInStartDrivingClick(){
        //Check gps status
        LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( manager.isProviderEnabled(LocationManager.GPS_PROVIDER) )
            requestCheckin();
        else
            AndroidUtils.buildAlertMessageNoGps(this);
    }

    private void requestCheckin() {
        if(RideValidator.isValid(drive)) {
            String url = UrlBuilder.create(Endpoint.RIDE).withArgs(getBundledRideId()).build();
            JSONObject params = RidesTranslator.getRideStatusParams(Ride.Status.CHECKIN);
            HSDJsonObjectRequest request = new HSDJsonObjectRequest(
                    Request.Method.PUT, url, params, mCheckinResponse, mCheckinErrorListener);

            if (loadingLock != null)
                loadingLock.show();

            mRequestQueue.add(request);
        }else {
            HelperFunctions.showLongToast("Something went wrong, please try again.", this);
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    Response.Listener<JSONObject> mCheckinResponse = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            ViewUtils.dismiss(loadingLock);

            if(JSONHelper.containsKey(jsonObject, "ride"))
            {
                JSONObject rideObject = JSONHelper.getJSONObject(jsonObject, "ride");
                drive = RidesTranslator.getRide(rideObject);
                if (RideValidator.isValid(drive)) {
                    mSession.setCurrentRide(drive);
                    Intent i = new Intent(DriveDetailsActivity.this, OnMyWayActivity.class);
                    startActivity(i);
                    finish();
                }
            }else{
                //TODO handle what happens when we get an invalid response
            }
        }
    };

    Response.ErrorListener mCheckinErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            ViewUtils.dismiss(loadingLock);
            volleyError.printStackTrace();
            HelperFunctions.showLongToast("We couldn't process your check-in request. Please call us to assist you.", DriveDetailsActivity.this);
            CallSupportDialog callSupportDialog = new CallSupportDialog(DriveDetailsActivity.this);
            callSupportDialog.show();
            //TODO add crashlytics
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.cancelRideActionMenu:
                CallSupportDialog callSupportDialog = new CallSupportDialog(DriveDetailsActivity.this);
                callSupportDialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drive_details, menu);
        return true;
    }

    //Gets the rideId that was passed in through bundle
    public String getBundledRideId() {
        String rideId = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(INTENT_KEY_RIDE_ID)) {
            rideId = (String) bundle.get(INTENT_KEY_RIDE_ID);
            HelperFunctions.setStringPreference(CURRENT_RIDE_ID_KEY, rideId, this);
        }
        return rideId;
    }
}