package com.hopskipdrive.hsd_android_v2.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.models.LoginData;
import com.hopskipdrive.hsd_android_v2.models.User;
import com.hopskipdrive.hsd_android_v2.models.translators.LoginDataTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.LoginDataValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.util.AndroidUtils;
import com.hopskipdrive.hsd_android_v2.util.GCMUtils;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;
import com.hopskipdrive.hsd_android_v2.util.TransparentProgressDialog;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import org.json.JSONObject;

import javax.inject.Inject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends Activity {

    @InjectView(R.id.email) EditText emailText;
    @InjectView(R.id.password) EditText passwordText;

    //Login
    int loginType = 0;
    String userName = "", password = "";

    //HTTP Request variables
    Map<String, String> parameters;
    StringRequest request;

    private LoginData loginData;
    User user;
    private TransparentProgressDialog loadingLock;

    @Inject RequestQueue mRequestQueue;
    @Inject HSDSession mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        HSDInject.inject(this);

        //Set our view up for the screen
        initialize();
    }

    private void initialize() {
        ButterKnife.inject(this);
        loadingLock = new TransparentProgressDialog(LoginActivity.this);
        loadingLock.setMessage("Please Wait...");
    }

    @OnFocusChange({R.id.email, R.id.password})
    protected void onFocusChanged(View view, boolean hasFocus) {
        if (!hasFocus)
            ViewUtils.hideKeyboard(this, view);
    }

    @OnClick(R.id.facebookLoginButton)
    protected void onFacebookLogin() {
        Toast.makeText(LoginActivity.this, "Feature coming soon!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.loginButton)
    protected void onLogin() {

        //Check if email is even in valid format
        if (TextUtils.isEmailValid(emailText.getText().toString())) {
            if (passwordText.getText().toString().length() > 0) {
                //Check if the device has internet access
                if (HelperFunctions.isNetworkAvailable(LoginActivity.this)) {
                    //0 = not logged in, 1 = Normal Login, 2 = Facebook Login.
                    loginType = 1;
                    userName = emailText.getText().toString().trim();
                    password = passwordText.getText().toString();
                    login(userName, password);
                }
                else {
                    HelperFunctions.showLongToast("Internet connection issues. Please check your internet connection.",
                        LoginActivity.this);
                }
            }
            else { HelperFunctions.showLongToast("Please enter a password.", LoginActivity.this); }
        }
        else { HelperFunctions.showLongToast("Please enter a valid email address.", LoginActivity.this); }
    }

    //Logs in the user through our server
    private void login(String userName, String password) {
        //Prepare the request
        String url = UrlBuilder.create(UrlBuilder.Endpoint.LOGIN).build();
        parameters = new HashMap<>();
        parameters.put("email", userName);
        parameters.put("password", password);

        //Create our HTTP Post request to our server
        request = new StringRequest(Request.Method.POST, url, loginResponse, errorListener){
            @Override
            protected Map<String, String> getParams() { return parameters; }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response){
                //Get our cookie and save it for future authentication
                HelperFunctions.setCookie(response.headers, LoginActivity.this);
                return super.parseNetworkResponse(response);
            }
        };

        if (loadingLock != null)
            loadingLock.show();

        mRequestQueue.add(request);
    }

    //Our login response from our server
    Response.Listener<String> loginResponse = new Response.Listener<String>(){
        @Override
        public void onResponse(String json) {
            ViewUtils.dismiss(loadingLock);
            loginData = LoginDataTranslator.getLoginData(json);
            if (LoginDataValidator.isValid(loginData)) {
                saveUserCredentials();
                registerDevice();
                setUpUser(loginData);
                goToRidesActivity();
            }
            else {
                ViewUtils.dismiss(loadingLock);
                HelperFunctions.showLongToast(loginData.getErrorMsg(), LoginActivity.this);
            }
        }
    };

    GoogleCloudMessaging gcm;
    String GCMRegistrationId;
    private void registerDevice() {
        if(AndroidUtils.isGooglePlayServicesAvailable(this)) {
            gcm = GoogleCloudMessaging.getInstance(this);
            GCMRegistrationId = GCMUtils.getRegistrationId(this);
            if(GCMRegistrationId.isEmpty())
                registerToGCMAndServer();
        }
    }

    //Registers device to Google Cloud Messaging in background
    private void registerToGCMAndServer() {
        new AsyncTask<Void, Void, String>() {
            @Override protected String doInBackground(Void... params) {
                try {
                    if (gcm == null)
                        gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);
                    GCMRegistrationId = gcm.register(getResources().getString(R.string.gcm_sender_id_string));
                    registerToOurServer();
                }
                catch (IOException e) { e.printStackTrace(); }
                return GCMRegistrationId;
            }
        }.execute(null,null,null);
    }

    private void registerToOurServer() {
        String url = UrlBuilder.create(UrlBuilder.Endpoint.DEVICE).build();
        JSONObject params = RidesTranslator.getDeviceParams("android", GCMRegistrationId);
        HSDJsonObjectRequest request = new HSDJsonObjectRequest(Request.Method.POST, url, params, deviceResponse, errorListener);
        mRequestQueue.add(request);
    }

    Response.Listener<JSONObject> deviceResponse = new Response.Listener<JSONObject>() {
        @Override public void onResponse(JSONObject jsonObject) { }
    };

    private void goToRidesActivity() {
        Intent intent = new Intent(LoginActivity.this, RidesActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveUserCredentials() {
        //Set username, passsword, loginType
        HelperFunctions.setStringPreference(getResources().getString(R.string.userNamePreference), userName, this);
        HelperFunctions.setStringPreference(getResources().getString(R.string.passwordPreference), password, this);
        HelperFunctions.setIntPreference(getResources().getString(R.string.loginTypePreference), loginType, this);

        //Set navigation preference
        String navPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference),this);
        if(navPreference == null || navPreference.isEmpty()){
            HelperFunctions.setStringPreference(getResources().getString(R.string.navigationPreference),
                    getResources().getString(R.string.googleMapsPreference), this);
        }
    }

    private void setUpUser(LoginData loginData) {
        user = mSession.getUser();
        if(user!= null && loginData != null) {
            user.setLoginData(loginData);

            if (user.getLoginData().getIsDriver()) {
                user.setCurrentRole(User.UserRole.DRIVER);
            }else if (user.getLoginData().getIsParent()) {
                user.setCurrentRole(User.UserRole.PARENT);
            }

        }else{
            Log.wtf("Error", "User is null or login data is null");
        }
    }

    //Our error response form our server
    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            ViewUtils.dismiss(loadingLock);
            HelperFunctions.showLongToast(
                "Invalid username or password. Please try again.", LoginActivity.this);
        }
    };

    @OnClick(R.id.forgotPwText)
    protected void onForgotPassword() {
        String url = getResources().getString(R.string.server_url_string)+ "/forgot";
        ViewUtils.openURL(this, url);
    }

    @OnClick(R.id.signUpText)
    protected void onSignUp() {
        String url = getResources().getString(R.string.server_url_string)+ "/qualify";
        ViewUtils.openURL(this, url);
    }

}