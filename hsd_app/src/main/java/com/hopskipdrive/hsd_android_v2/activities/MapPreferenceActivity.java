package com.hopskipdrive.hsd_android_v2.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MapPreferenceActivity extends AppCompatActivity {
    @InjectView(R.id.map_preference_toolbar) Toolbar mToolbar;
    @InjectView(R.id.googleMapsTab) CardView googleMapsTab;
    @InjectView(R.id.googleMapsCheck) ImageView googleMapsCheck;
    @InjectView(R.id.wazeTab) CardView wazeTab;
    @InjectView(R.id.wazeCheck) ImageView wazeCheck;
    boolean isGoogleInstalled, isWazeInstalled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_preference);
        ButterKnife.inject(this);

        //Set our toolbar header view
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        //Check if navigation apps are installed
        isGoogleInstalled = isAppInstalled("com.google.android.apps.maps");
        isWazeInstalled = isAppInstalled("com.waze");

        //Make google maps visible
        if(isGoogleInstalled)
            googleMapsTab.setVisibility(View.VISIBLE);

        //Make waze visible
        if(isWazeInstalled)
            wazeTab.setVisibility(View.VISIBLE);

        //Add image check to the user's saved navigation preference
        String navPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference),this);
        if(navPreference.equals(getResources().getString(R.string.wazePreference))){
            wazeCheck.setVisibility(View.VISIBLE);
            googleMapsCheck.setVisibility(View.INVISIBLE);
        }else if(navPreference.equals(getResources().getString(R.string.googleMapsPreference))){
            googleMapsCheck.setVisibility(View.VISIBLE);
            wazeCheck.setVisibility(View.INVISIBLE);
        }else{
            HelperFunctions.setStringPreference(getResources().getString(R.string.navigationPreference),
                    getResources().getString(R.string.googleMapsPreference), this);
            googleMapsCheck.setVisibility(View.VISIBLE);
            wazeCheck.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.googleMapsTab)
    protected void onGoogleMapsTabClick(){
        HelperFunctions.setStringPreference(getResources().getString(R.string.navigationPreference),
                getResources().getString(R.string.googleMapsPreference), this);
        googleMapsCheck.setVisibility(View.VISIBLE);
        wazeCheck.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.wazeTab)
    protected void onWazeTabClick(){
        HelperFunctions.setStringPreference(getResources().getString(R.string.navigationPreference),
                getResources().getString(R.string.wazePreference), this);
        wazeCheck.setVisibility(View.VISIBLE);
        googleMapsCheck.setVisibility(View.INVISIBLE);
    }

    //Function that tells us if an app is installed on device
    private boolean isAppInstalled(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map_preference, menu);
        menu.getItem(menu.size()-1).setVisible(false); //make hamburger menu disappear
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //back button pressed
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
