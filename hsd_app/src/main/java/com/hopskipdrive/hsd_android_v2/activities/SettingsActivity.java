package com.hopskipdrive.hsd_android_v2.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.dialogs.ContactUsDialog;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import java.io.File;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.ButterKnife;

public class SettingsActivity extends Activity {
    @InjectView(R.id.settingsIcon) ImageView settingsIcon;
    @InjectView(R.id.drivingMapPreference) TextView mapPreferenceText;

    @Inject HSDSession mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        HSDInject.inject(this);
        ButterKnife.inject(this);
        settingsIcon.setImageResource(R.mipmap.settings_active2x);
        setNavigationPreference();
    }

    //Function that saves the user's navigation preference
    private void setNavigationPreference() {
        String navPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference),this);
        if(navPreference.equals(getResources().getString(R.string.wazePreference))){
            mapPreferenceText.setText("Waze");
        }else if(navPreference.equals(getResources().getString(R.string.googleMapsPreference))){
            mapPreferenceText.setText("Google Maps");
        }else{
            HelperFunctions.setStringPreference(getResources().getString(R.string.navigationPreference),
                    getResources().getString(R.string.googleMapsPreference), this);
            mapPreferenceText.setText("Google Maps");
        }
    }

    @OnClick(R.id.manageAccountTab)
    protected void onManageAccountClick(){
        String url = getResources().getString(R.string.server_url_string)+"/account";
        ViewUtils.openURL(this,url);
    }

    @OnClick(R.id.mapPreferenceTab)
    protected void onMapPreferenceClick(){
        Intent i = new Intent(SettingsActivity.this, MapPreferenceActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.logoutTab)
    protected void onLogoutClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setMessage(
                getResources().getString(R.string.logout_prompt_string))
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                logoutUser();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void logoutUser() {
        //Clear session
        mSession.clearSession();

        //Clear shared preferences
        PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();

        //Clear cache
        try {
            File dir = this.getCacheDir();
            if (dir != null && dir.isDirectory())
                deleteDir(dir);
        } catch (Exception e) { e.printStackTrace(); }

        //Go to login Activity
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    //Recursive function to delete all the directories in cache
    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success)
                    return false;
            }
        }
        return dir.delete();    //The directory is now empty so delete it
    }

    @OnClick(R.id.aboutTab)
    protected void onAboutClick(){
        String url = "http://www.hopskipdrive.com/about";
        ViewUtils.openURL(this, url);
    }

    @OnClick(R.id.privacyPolicyTab)
    protected void onPrivacyPolicyClick(){
        String url = "http://www.hopskipdrive.com/privacy/";
        ViewUtils.openURL(this, url);
    }

    @OnClick(R.id.contactUsTab)
    protected void onContactUsClick(){
        ContactUsDialog contactUsDialog = new ContactUsDialog(SettingsActivity.this);
        contactUsDialog.show();
    }

    @OnClick(R.id.ridesIcon)
    protected void onRidesClick() {
        Intent i = new Intent(SettingsActivity.this, RidesActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.supportIcon)
    protected void onSupportClick() {
        CallSupportDialog callSupportDialog = new CallSupportDialog(SettingsActivity.this);
        callSupportDialog.show();
    }

    @OnClick(R.id.cpucText)
    protected void onCPUCClick(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.cpuc_description);
        builder.setPositiveButton("OK", null);
        AlertDialog dialog = builder.show();

        // Must call show() prior to fetching text view
        TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }
}
