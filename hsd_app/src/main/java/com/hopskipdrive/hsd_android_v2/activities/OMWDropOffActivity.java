package com.hopskipdrive.hsd_android_v2.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.dialogs.TextParentRidersDialog;
import com.hopskipdrive.hsd_android_v2.fragments.HSDMapFragment;
import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.translators.AddressTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;
import com.hopskipdrive.hsd_android_v2.timer_tasks.HeartBeatTaskManager;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import com.hopskipdrive.hsd_android_v2.util.TransparentProgressDialog;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder.Endpoint;
import com.hopskipdrive.hsd_android_v2.util.ViewHelper;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class OMWDropOffActivity extends AppCompatActivity {
    private static final String INTENT_KEY_RIDE_ID = "Ride.Id";

    @InjectView(R.id.OMWDropoffToolbar) Toolbar mToolbar;
    @InjectView(R.id.omw_drop_off_PassengerList) LinearLayout mPassengerLayout;

    @InjectView(R.id.ride_pickup_at_text) TextView ridePickupTimeAtView;
    @InjectView(R.id.ride_pickup_time) TextView ridePickupTimeView;
    @InjectView(R.id.ride_pickup_address_name) TextView ridePickupAddressNameView;
    @InjectView(R.id.ride_pickup_date) TextView ridePickupDateView;
    @InjectView(R.id.ride_pickup_address) TextView ridePickupAddressView;
    @InjectView(R.id.ride_dropoff_time) TextView rideDropoffTimeView;
    @InjectView(R.id.ride_dropoff_address_name) TextView rideDropoffAddressNameView;
    @InjectView(R.id.ride_dropoff_address) TextView rideDropoffAddressView;

    @InjectView(R.id.omw_drop_off_pickup_notes) TextView pickupNotesView;
    @InjectView(R.id.omw_drop_off_dropoff_notes) TextView dropoffNotesView;
    @InjectView(R.id.omw_drop_off_text_parent_rider) TextView textParentRiderView;
    @InjectView(R.id.omw_dropoff_title) TextView mTitleView;
    @InjectView(R.id.omw_dropoff_navigation_button) ImageView mNavigationView;

    @Inject HSDSession mSession;
    @Inject RequestQueue mRequestQueue;
    @Inject LocationUpdateServiceManager mLocationUpdateManager;
    @Inject HeartBeatTaskManager mHeartBeatTaskManager;

    HSDMapFragment mMapFragment;
    TransparentProgressDialog loadingLock;
    Ride drive;
    String navigationPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_omw_drop_off);
        initialize();
        initMapView();
        setOMWToDropOffViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!RideValidator.isValid(drive)){
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void initialize() {
        HSDInject.inject(this);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        loadingLock = new TransparentProgressDialog(this);
        loadingLock.setMessage("Please Wait...");
        navigationPreference = HelperFunctions.getStringPreference(getResources().getString(R.string.navigationPreference), this);
        drive = mSession.getCurrentRide();
    }

    private void initMapView() {
        //Set their preferred navigation app image
        if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
            mNavigationView.setImageResource(R.mipmap.waze_icon_transparent);
        else
            mNavigationView.setImageResource(R.mipmap.google_map_icon_transparent);

        //Set map
        mMapFragment = new HSDMapFragment();
        mMapFragment.setOnResumeListener(new HSDMapFragment.OnResumeListener() {
            @Override
            public void onResume(HSDMapFragment fragment) {
                if (fragment != null && fragment.getMap() != null) {
                    fragment.getMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            initMapLocation();
                        }
                    });
                }
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.omw_dropoff_map_container, mMapFragment).commit();
    }

    private void initMapLocation() {
        if (mMapFragment != null) {
            GoogleMap map = mMapFragment.getMap();
            if (map != null && RideValidator.isValid(drive) &&
                    AddressValidator.isValid(drive.getSaddr()) && AddressValidator.isValid(drive.getDaddr()))
            {
                int dimension = Math.min(mMapFragment.getView().getWidth(), mMapFragment.getView().getHeight());
                int padding = (int) (dimension * 0.3);

                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.getUiSettings().setZoomControlsEnabled(true);

                // Set Map boundaries
                LatLngBounds.Builder builder = LatLngBounds.builder();
                builder.include(new LatLng(drive.getSaddr().getLat(), drive.getSaddr().getLng()));
                builder.include(new LatLng(drive.getDaddr().getLat(), drive.getDaddr().getLng()));
                LatLngBounds bounds = builder.build();
                CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.moveCamera(update);

                // Add map markers
                map.addMarker(getMarker(drive.getSaddr(), R.color.orange_color, "Pick Up"));
                map.addMarker(getMarker(drive.getDaddr(), R.color.hsd_blue, "Drop Off")).showInfoWindow();
            }else{
                HelperFunctions.restartApp(this);
                finish();
            }
        }
    }

    private MarkerOptions getMarker(Address address, @ColorRes int colorResid, String title) {
        float[] hsv = new float[3];
        Color.colorToHSV(getResources().getColor(colorResid), hsv);
        float hue = hsv[0];
        return new MarkerOptions()
                .position(new LatLng(address.getLat(), address.getLng()))
                .icon(BitmapDescriptorFactory.defaultMarker(hue))
                .title(title);
    }

    @OnClick(R.id.omw_dropoff_navigation_button)
    protected void onOMWDropoffNavigationButtonClicked(){
        if(RideValidator.isValid(drive) && AddressValidator.isValid(drive.getDaddr())) {
            if (navigationPreference.equals(getResources().getString(R.string.wazePreference)))
                ViewUtils.openAppForDirections(this, false, String.valueOf(drive.getDaddr().getLat()), String.valueOf(drive.getDaddr().getLng()));
            else
                ViewUtils.openAppForDirections(this,true, String.valueOf(drive.getDaddr().getLat()), String.valueOf(drive.getDaddr().getLng()));
        }
    }

    private void setOMWToDropOffViews() {
        if(RideValidator.isValid(drive)){
            mTitleView.setText(drive.getName());
            setPickupDropoffCardView(drive);
            setPassengerInfoCardView(drive);
            ViewHelper.setText(pickupNotesView, drive.getPickUpNotes(), getString(R.string.pickup_notes_none));
            ViewHelper.setText(dropoffNotesView, drive.getDropOffNotes(), getString(R.string.drop_off_notes_none));
        }
    }

    //Sets the pick up and drop off card view with ride data
    private void setPickupDropoffCardView(Ride drive)
    {
        //Actual Pick Up time
        ridePickupTimeAtView.setText("Picked up at");
        String actualPickupTime = HelperFunctions.convertTSToRegularTime(drive.getActual_pickup_ts());
        ridePickupTimeView.setText(actualPickupTime);

        //Pick Up name & address
        if(AddressValidator.isValid(drive.getSaddr())){
            ridePickupAddressNameView.setText(drive.getSaddr().getName());
            ridePickupAddressView.setText(AddressTranslator.getDisplayText(drive.getSaddr()));
        }

        //Pick Up date
        String[] driveTimes;
        if(drive.getPickUpDate() != null) {
            driveTimes = HelperFunctions.getRegularTime(drive.getPickUpDate().toString());
            ridePickupDateView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
        }

        //Drop off time
        if(drive.getDropoff() != null) {
            String dropoffTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getDropoff());
            rideDropoffTimeView.setText(dropoffTime);
        }

        //Drop off name & address
        if(AddressValidator.isValid(drive.getDaddr())){
            rideDropoffAddressNameView.setText(drive.getDaddr().getName());
            rideDropoffAddressView.setText(AddressTranslator.getDisplayText(drive.getDaddr()));
        }
    }

    //Sets the data for the passenger card view
    private void setPassengerInfoCardView(Ride drive) {
        List<Rider> riders = drive.getRiders();
        if (RiderValidator.isValid(riders)) {
            if (riders.size() == 1)
                textParentRiderView.setText("Text Parent & Rider");
            else
                textParentRiderView.setText("Text Parent & Riders");

            for (int i = 0; i < riders.size(); i++)
                ViewUtils.addPassengerList(OMWDropOffActivity.this, mPassengerLayout, riders.get(i));
        }
    }

    @OnClick(R.id.drop_off_button)
    protected void onDropOffClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                getResources().getString(R.string.drop_off_prompt_string))
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mLocationUpdateManager.stopTracking();
                                mHeartBeatTaskManager.stopHeartBeat();
                                dialog.cancel();
                                requestDropOff();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestDropOff() {
        if(RideValidator.isValid(drive)) {
            String url = UrlBuilder.create(Endpoint.RIDE).withArgs(drive.get_id()).build();
            JSONObject params = RidesTranslator.getRideStatusParams(Ride.Status.DELIVERED);
            HSDJsonObjectRequest request = new HSDJsonObjectRequest(
                    Request.Method.PUT, url, params, mDropoffResponse, mDropoffErrorListener);

            if (loadingLock != null)
                loadingLock.show();

            mRequestQueue.add(request);
        }else{
            HelperFunctions.showLongToast("Something went wrong, please try again.", this);
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    Response.Listener<JSONObject> mDropoffResponse = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            ViewUtils.dismiss(loadingLock);

            if(JSONHelper.containsKey(jsonObject, "ride"))
            {
                JSONObject rideObject = JSONHelper.getJSONObject(jsonObject, "ride");
                drive = RidesTranslator.getRide(rideObject);
                if (RideValidator.isValid(drive)) {
                    mSession.setCurrentRide(drive);
                    Intent i = new Intent(OMWDropOffActivity.this, RideCompletedActivity.class);
                    i.putExtra(INTENT_KEY_RIDE_ID, drive.get_id());
                    startActivity(i);
                    finish();
                }
            }else{
                //TODO handle what happens when we get an invalid response
            }
        }
    };

    Response.ErrorListener mDropoffErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            ViewUtils.dismiss(loadingLock);
            volleyError.printStackTrace();
            HelperFunctions.showLongToast("We couldn't process your dropoff request. Please call us to assist you.", OMWDropOffActivity.this);
            CallSupportDialog callSupportDialog = new CallSupportDialog(OMWDropOffActivity.this);
            callSupportDialog.show();
            //TODO add crashlytics
        }
    };

    @OnClick(R.id.omw_drop_off_text_parent_kids)
    protected void onTextParentKidsClick() {
        if(RideValidator.isValid(drive)){
            TextParentRidersDialog dialog = new TextParentRidersDialog(OMWDropOffActivity.this, drive.getPhone_numbers());
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_omw_drop_off, menu);
        menu.getItem(menu.size()-1).setVisible(false); //make hamburger menu disappear
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //back button pressed
                Intent intent = new Intent(this, RidesActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
