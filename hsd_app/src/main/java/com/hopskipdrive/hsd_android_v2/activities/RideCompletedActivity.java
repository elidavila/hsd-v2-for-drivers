package com.hopskipdrive.hsd_android_v2.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.translators.AddressTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RidesValidator;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RideCompletedActivity extends AppCompatActivity {
    @InjectView(R.id.ride_completed_toolbar) Toolbar mToolbar;
    @InjectView(R.id.ridesIcon) ImageView ridesIcon;

    @InjectView(R.id.ride_pickup_at_text) TextView ridePickupTimeAtView;
    @InjectView(R.id.ride_dropoff_at_text) TextView rideDropoffTimeAtView;
    @InjectView(R.id.ride_pickup_time) TextView ridePickupTimeView;
    @InjectView(R.id.ride_pickup_address_name) TextView ridePickupAddressNameView;
    @InjectView(R.id.ride_pickup_date) TextView ridePickupDateView;
    @InjectView(R.id.ride_pickup_address) TextView ridePickupAddressView;

    @InjectView(R.id.ride_actual_dropoff_time) TextView rideActualDropoffTimeView;
    @InjectView(R.id.ride_dropoff_time) TextView rideDropoffTimeView;
    @InjectView(R.id.ride_dropoff_address_name) TextView rideDropoffAddressNameView;
    @InjectView(R.id.ride_dropoff_address) TextView rideDropoffAddressView;
    @InjectView(R.id.ride_completed_title) TextView rideCompletedTitleView;

    @InjectView(R.id.ride_completed_dollars_earned) TextView dollarsEarnedView;
    @InjectView(R.id.ride_completed_miles_traveled) TextView milesTraveledView;
    @InjectView(R.id.ride_completed_travel_time) TextView travelTimeView;

    @InjectView(R.id.next_ride_date_time) TextView nextRideTimeView;

    @InjectView(R.id.mini_no_rides_layout) LinearLayout noRidesView;
    @InjectView(R.id.next_ride_layout) LinearLayout nextRideView;

    @Inject HSDSession mSession;

    Ride drive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_completed);
        initialize();
        setRideCompletedView();
        mSession.setCurrentRide(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!RideValidator.isValid(drive)){
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void initialize() {
        HSDInject.inject(this);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        drive = mSession.getCurrentRide();
    }

    private void setRideCompletedView() {
        if(RideValidator.isValid(drive)){
            rideCompletedTitleView.setText(drive.getName());
            dollarsEarnedView.setText(TextUtils.convertCentsToDollars(drive.getDriver_fee()));
            milesTraveledView.setText(String.valueOf(drive.getActual_distance()));
            travelTimeView.setText(drive.getActual_time());
            setPickupDropoffCardView(drive);
            setUpcomingRideView(drive);
        }
        ridesIcon.setImageResource(R.mipmap.rides_active2x); //Set rides footer icon to active
    }

    //Sets the pick up and drop off card view with ride data
    private void setPickupDropoffCardView(Ride drive)
    {
        //Actual Pick Up time
        ridePickupTimeAtView.setText("Picked up at");
        String actualPickupTime = HelperFunctions.convertTSToRegularTime(drive.getActual_pickup_ts());
        ridePickupTimeView.setText(actualPickupTime);

        //Pick Up name & address
        if(AddressValidator.isValid(drive.getSaddr())){
            ridePickupAddressNameView.setText(drive.getSaddr().getName());
            ridePickupAddressView.setText(AddressTranslator.getDisplayText(drive.getSaddr()));
        }

        //Pick Up date
        String[] driveTimes;
        if(drive.getPickUpDate() != null) {
            driveTimes = HelperFunctions.getRegularTime(drive.getPickUpDate().toString());
            ridePickupDateView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
        }

        //Actual drop off time
        rideDropoffTimeAtView.setText("Dropped off at");
        String actualDropoffTime = HelperFunctions.convertTSToRegularTime(drive.getActual_dropoff_ts());
        rideActualDropoffTimeView.setText(actualDropoffTime);

        //Drop off time
        if(drive.getDropoff() != null) {
            String dropoffTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getDropoff());
            rideDropoffTimeView.setText(dropoffTime);
        }

        //Drop off name & address
        if(AddressValidator.isValid(drive.getDaddr())){
            rideDropoffAddressNameView.setText(drive.getDaddr().getName());
            rideDropoffAddressView.setText(AddressTranslator.getDisplayText(drive.getDaddr()));
        }
    }

    private void setUpcomingRideView(Ride currentRide) {
        List<Ride> upcomingRides = mSession.getUpcomingRides();
        if (RidesValidator.isValid(upcomingRides)) {
            nextRideView.setVisibility(View.VISIBLE);
            noRidesView.setVisibility(View.GONE);

            if(upcomingRides.get(0).get_id().equals(currentRide.get_id()))
                mSession.getUpcomingRides().remove(0);

            if(!RidesValidator.isEmpty(mSession.getUpcomingRides())){
                //Set next ride date
                upcomingRides = mSession.getUpcomingRides();
                String[] driveTimes = HelperFunctions.getRegularTime(upcomingRides.get(0).getPickUpDate().toString());
                if(driveTimes != null && driveTimes.length >= 4)
                    nextRideTimeView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
                else
                    nextRideTimeView.setText(upcomingRides.get(0).getDate());
            }else{
                noRidesView.setVisibility(View.VISIBLE);
                nextRideView.setVisibility(View.GONE);
            }
        }else{
            noRidesView.setVisibility(View.VISIBLE);
            nextRideView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.mini_update_availability_button)
    protected void onMiniUpdateAvailabilityClick() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.server_url_string) + "/schedule"));
        startActivity(intent);
    }

    @OnClick(R.id.go_to_ride_list_button)
    protected void onGoToRideListClick() {
        Intent i = new Intent(this, RidesActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.supportIcon)
    protected void onSupportClick() {
        CallSupportDialog callSupportDialog = new CallSupportDialog(this);
        callSupportDialog.show();
    }

    @OnClick(R.id.settingsIcon)
    protected void onSettingsClick() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ride_completed, menu);
        menu.getItem(menu.size()-1).setVisible(false); //make hamburger menu disappear
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
