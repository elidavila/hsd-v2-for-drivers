package com.hopskipdrive.hsd_android_v2.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.astuetz.PagerSlidingTabStrip;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.adapters.RidesPagerAdapter;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;

public class RidesActivity extends AppCompatActivity {
    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.pager) ViewPager mPager;
    @InjectView(R.id.indicator) PagerSlidingTabStrip mIndicator;
    @InjectView(R.id.ridesIcon) ImageView ridesIcon;
    RidesPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rides);
        HSDInject.inject(this);
        ButterKnife.inject(this);

        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        mPagerAdapter = new RidesPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mIndicator.setShouldExpand(true);
        mIndicator.setAllCaps(false);
        mIndicator.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/raleway_semi_bold.ttf"), R.style.AppTheme);
        mIndicator.setIndicatorColor(getResources().getColor(R.color.orange_color));
        mIndicator.setUnderlineColor(getResources().getColor(R.color.orange_color));
        mIndicator.setDividerColor(getResources().getColor(R.color.ride_border_color));
        mIndicator.setViewPager(mPager);
        ridesIcon.setImageResource(R.mipmap.rides_active2x);
    }

    @OnClick(R.id.supportIcon)
    protected void onSupportClick() {
        CallSupportDialog callSupportDialog = new CallSupportDialog(this);
        callSupportDialog.show();
    }

    @OnClick(R.id.settingsIcon)
    protected void onSettingsClick() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        finish();
    }
}
