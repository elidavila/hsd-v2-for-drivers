package com.hopskipdrive.hsd_android_v2.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.dialogs.CallSupportDialog;
import com.hopskipdrive.hsd_android_v2.dialogs.TextParentRidersDialog;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.translators.AddressTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import com.hopskipdrive.hsd_android_v2.util.TransparentProgressDialog;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder;
import com.hopskipdrive.hsd_android_v2.util.UrlBuilder.Endpoint;
import com.hopskipdrive.hsd_android_v2.util.ViewHelper;
import com.hopskipdrive.hsd_android_v2.util.ViewUtils;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PickUpActivity extends AppCompatActivity {
    @InjectView(R.id.pick_up_toolbar) Toolbar mToolbar;
    @InjectView(R.id.pick_up_passenger_list) LinearLayout mPassengerLayout;

    @InjectView(R.id.ride_pickup_time) TextView ridePickupTimeView;
    @InjectView(R.id.ride_pickup_address_name) TextView ridePickupAddressNameView;
    @InjectView(R.id.ride_pickup_date) TextView ridePickupDateView;
    @InjectView(R.id.ride_pickup_address) TextView ridePickupAddressView;
    @InjectView(R.id.ride_dropoff_time) TextView rideDropoffTimeView;
    @InjectView(R.id.ride_dropoff_address_name) TextView rideDropoffAddressNameView;
    @InjectView(R.id.ride_dropoff_address) TextView rideDropoffAddressView;

    @InjectView(R.id.pick_up_title) TextView pickupTitleView;
    @InjectView(R.id.pick_up_pickup_notes) TextView pickupNotesView;
    @InjectView(R.id.pick_up_dropoff_notes) TextView dropoffNotesView;
    @InjectView(R.id.pick_up_text_parent_rider) TextView textParentRiderView;

    @Inject HSDSession mSession;
    @Inject RequestQueue mRequestQueue;

    TransparentProgressDialog loadingLock;
    Ride drive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up);
        initialize();
        setPickupRideDataViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!RideValidator.isValid(drive)){
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void initialize() {
        HSDInject.inject(this);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        loadingLock = new TransparentProgressDialog(this);
        loadingLock.setMessage("Please Wait...");
        drive = mSession.getCurrentRide();
    }

    private void setPickupRideDataViews() {
        if(RideValidator.isValid(drive)){
            pickupTitleView.setText(drive.getName());
            setPickupDropoffCardView(drive);
            setPassengerInfoCardView(drive);
            ViewHelper.setText(pickupNotesView, drive.getPickUpNotes(), getString(R.string.pickup_notes_none));
            ViewHelper.setText(dropoffNotesView, drive.getDropOffNotes(), getString(R.string.drop_off_notes_none));
        }
    }

    //Sets the pick up and drop off card view with ride data
    private void setPickupDropoffCardView(Ride drive)
    {
        //Pick Up time
        if(drive.getPickup() != null) {
            String pickupTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getPickup());
            ridePickupTimeView.setText(pickupTime);
        }

        //Pick Up name & address
        if(AddressValidator.isValid(drive.getSaddr())){
            ridePickupAddressNameView.setText(drive.getSaddr().getName());
            ridePickupAddressView.setText(AddressTranslator.getDisplayText(drive.getSaddr()));
        }

        //Pick Up date
        String[] driveTimes;
        if(drive.getPickUpDate() != null) {
            driveTimes = HelperFunctions.getRegularTime(drive.getPickUpDate().toString());
            ridePickupDateView.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
        }

        //Drop off time
        if(drive.getDropoff() != null) {
            String dropoffTime = HelperFunctions.convertMilitaryTimeToRegularTime(drive.getDropoff());
            rideDropoffTimeView.setText(dropoffTime);
        }

        //Drop off name & address
        if(AddressValidator.isValid(drive.getDaddr())){
            rideDropoffAddressNameView.setText(drive.getDaddr().getName());
            rideDropoffAddressView.setText(AddressTranslator.getDisplayText(drive.getDaddr()));
        }
    }

    //Sets the data for the passenger card view
    private void setPassengerInfoCardView(Ride drive) {
        List<Rider> riders = drive.getRiders();
        if (RiderValidator.isValid(riders)) {
            if (riders.size() == 1)
                textParentRiderView.setText("Text Parent & Rider");
            else
                textParentRiderView.setText("Text Parent & Riders");

            for (int i = 0; i < riders.size(); i++)
                ViewUtils.addPassengerList(PickUpActivity.this, mPassengerLayout, riders.get(i));
        }
    }

    @OnClick(R.id.pick_up_button)
    protected void onPickUpClick() {
        String codeWords = "";
        if(RideValidator.isValid(drive)) {
            List<Rider> riders = drive.getRiders();
            for (int i = 0; i < riders.size(); i++)
                if (i != riders.size() - 1)
                    codeWords += riders.get(i).getPassword() + ", ";
                else
                    codeWords += riders.get(i).getPassword();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("The code word is " + codeWords + ". Have you picked up using the code word?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    requestPickUp();
                                }
                            })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }else {
            HelperFunctions.showLongToast("Something went wrong, please try again.", this);
            HelperFunctions.restartApp(this);
            finish();
        }
    }

    private void requestPickUp() {
        String url = UrlBuilder.create(Endpoint.RIDE).withArgs(drive.get_id()).build();
        JSONObject params = RidesTranslator.getRideStatusParams(Ride.Status.PICKUP);
        HSDJsonObjectRequest request = new HSDJsonObjectRequest(
                Request.Method.PUT, url, params,  mPickupResponse, mPickupErrorListener);

        if (loadingLock != null)
            loadingLock.show();

        mRequestQueue.add(request);
    }

    Response.Listener<JSONObject> mPickupResponse = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            ViewUtils.dismiss(loadingLock);

            if(JSONHelper.containsKey(jsonObject, "ride"))
            {
                JSONObject rideObject = JSONHelper.getJSONObject(jsonObject, "ride");
                drive = RidesTranslator.getRide(rideObject);
                if (RideValidator.isValid(drive)) {
                    mSession.setCurrentRide(drive);
                    Intent i = new Intent(PickUpActivity.this, OMWDropOffActivity.class);
                    startActivity(i);
                    finish();
                }
            }else{
                //TODO handle what happens when we get an invalid response
            }
        }
    };

    Response.ErrorListener mPickupErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            ViewUtils.dismiss(loadingLock);
            volleyError.printStackTrace();
            HelperFunctions.showLongToast("We couldn't process your pickup request. Please call us to assist you.", PickUpActivity.this);
            CallSupportDialog callSupportDialog = new CallSupportDialog(PickUpActivity.this);
            callSupportDialog.show();
            //TODO add crashlytics
        }
    };

    @OnClick(R.id.pick_up_text_parent_rider_button)
    protected void onTextParentKidsClick() {
        if(RideValidator.isValid(drive)){
            TextParentRidersDialog dialog = new TextParentRidersDialog(PickUpActivity.this, drive.getPhone_numbers());
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pick_up, menu);
        menu.getItem(menu.size()-1).setVisible(false); //make hamburger menu disappear
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
