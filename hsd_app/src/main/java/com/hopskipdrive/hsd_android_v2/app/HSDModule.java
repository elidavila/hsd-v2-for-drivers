package com.hopskipdrive.hsd_android_v2.app;

import android.content.Context;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.hopskipdrive.hsd_android_v2.gcm.GcmIntentService;
import com.hopskipdrive.hsd_android_v2.net.HSDJsonObjectRequest;
import com.hopskipdrive.hsd_android_v2.activities.DriveDetailsActivity;
import com.hopskipdrive.hsd_android_v2.activities.LoginActivity;
import com.hopskipdrive.hsd_android_v2.activities.OMWDropOffActivity;
import com.hopskipdrive.hsd_android_v2.activities.OnMyWayActivity;
import com.hopskipdrive.hsd_android_v2.activities.PickUpActivity;
import com.hopskipdrive.hsd_android_v2.activities.RideCompletedActivity;
import com.hopskipdrive.hsd_android_v2.activities.RidesActivity;
import com.hopskipdrive.hsd_android_v2.activities.SettingsActivity;
import com.hopskipdrive.hsd_android_v2.activities.SplashActivity;
import com.hopskipdrive.hsd_android_v2.adapters.RidesAdapter;
import com.hopskipdrive.hsd_android_v2.adapters.RidesPagerAdapter;
import com.hopskipdrive.hsd_android_v2.fragments.RidesListFragment;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateListener;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;
import com.hopskipdrive.hsd_android_v2.timer_tasks.HeartBeatTask;
import com.hopskipdrive.hsd_android_v2.timer_tasks.HeartBeatTaskManager;
import com.hopskipdrive.hsd_android_v2.util.SessionUtils;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module (
    injects = {
            DriveDetailsActivity.class,
            GcmIntentService.class,
            HeartBeatTask.class,
            HSDApplication.class,
            HSDJsonObjectRequest.class,
            LocationUpdateListener.class,
            LoginActivity.class,
            OMWDropOffActivity.class,
            OnMyWayActivity.class,
            PickUpActivity.class,
            RideCompletedActivity.class,
            RidesActivity.class,
            RidesAdapter.class,
            RidesListFragment.class,
            RidesPagerAdapter.class,
            SettingsActivity.class,
            SessionUtils.class,
            SplashActivity.class
    }
)
public class HSDModule {

    private final Context mContext;

    public HSDModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return mContext;
    }

    @Singleton
    @Provides
    RequestQueue provideRequestQueue(Context context) {
        return Volley.newRequestQueue(context);
    }

    @Singleton
    @Provides
    HSDSession provideSession() {
        return new HSDSession();
    }

    @Singleton
    @Provides
    LocationUpdateServiceManager provideLocationUpdateServiceManager() {
        return new LocationUpdateServiceManager();
    }

    @Singleton
    @Provides
    HeartBeatTaskManager provideHeartBeatTaskManager() {
        return new HeartBeatTaskManager();
    }
}
