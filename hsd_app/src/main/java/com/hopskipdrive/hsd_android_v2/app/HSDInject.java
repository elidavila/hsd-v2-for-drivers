package com.hopskipdrive.hsd_android_v2.app;

import dagger.ObjectGraph;

public class HSDInject {

    private static ObjectGraph sObjectGraph;

    public static void initialize(ObjectGraph graph) {
        sObjectGraph = graph;
    }

    public static <T> T inject(T instance) {
        return sObjectGraph.inject(instance);
    }
}
