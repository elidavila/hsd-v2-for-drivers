package com.hopskipdrive.hsd_android_v2.app;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.hopskipdrive.hsd_android_v2.BuildConfig;
import com.hopskipdrive.hsd_android_v2.models.validators.UserValidator;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;
import com.hopskipdrive.hsd_android_v2.util.SessionUtils;

import dagger.ObjectGraph;
import io.fabric.sdk.android.Fabric;

import javax.inject.Inject;

public class HSDApplication extends Application {

    @Inject LocationUpdateServiceManager mLocationUpdateServiceManager;
    @Inject HSDSession mSession;

    @Override
    public void onCreate() {
        super.onCreate();

        HSDInject.initialize(ObjectGraph.create(new HSDModule(this)));
        HSDInject.inject(this);

        Crashlytics crashlytics = new Crashlytics.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, crashlytics);

        mLocationUpdateServiceManager.init(this);

        if(!UserValidator.isValid(mSession.getUser())) {
            SessionUtils sessionUtils = new SessionUtils(getApplicationContext());
            sessionUtils.cacheUserSession();
        }
    }
}
