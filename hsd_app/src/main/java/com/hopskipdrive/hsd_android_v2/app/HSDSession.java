package com.hopskipdrive.hsd_android_v2.app;

import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.User;

import java.util.List;

public class HSDSession {

    private User user;
    private Ride ride;
    private List<Ride> upcomingRides;
    private List<Ride> completedRides;

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public Ride getCurrentRide(){
        return ride;
    }
    public void setCurrentRide(Ride ride){
        this.ride = ride;
    }

    public void setUpcomingRides(List<Ride> upcomingRides){this.upcomingRides = upcomingRides;}
    public List<Ride> getUpcomingRides(){return upcomingRides;}

    public void setCompletedRides(List<Ride> completedRides){this.completedRides = completedRides;}
    public List<Ride> getCompletedRides(){return completedRides;}

    public void clearSession(){
        user = null;
        ride = null;
        upcomingRides = null;
        completedRides = null;
    }

}
