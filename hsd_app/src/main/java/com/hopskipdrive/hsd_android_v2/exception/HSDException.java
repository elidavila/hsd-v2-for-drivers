package com.hopskipdrive.hsd_android_v2.exception;

public class HSDException extends Exception {
    public HSDException(String s) {
        super(s);
    }

    public HSDException(String format, Object... args) {
        this(String.format(format, args));
    }
}
