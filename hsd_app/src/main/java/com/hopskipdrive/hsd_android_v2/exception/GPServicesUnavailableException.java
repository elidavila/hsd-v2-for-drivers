package com.hopskipdrive.hsd_android_v2.exception;

public class GPServicesUnavailableException extends Exception {
    public GPServicesUnavailableException(String s) {
        super(s);
    }
}
