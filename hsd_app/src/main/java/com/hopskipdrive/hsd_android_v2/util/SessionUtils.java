package com.hopskipdrive.hsd_android_v2.util;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.models.LoginData;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.User;
import com.hopskipdrive.hsd_android_v2.models.translators.LoginDataTranslator;
import com.hopskipdrive.hsd_android_v2.models.translators.RidesTranslator;
import com.hopskipdrive.hsd_android_v2.models.validators.LoginDataValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RidesValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.UserValidator;
import com.hopskipdrive.hsd_android_v2.services.LocationUpdateServiceManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class SessionUtils {
    @Inject RequestQueue mRequestQueue;
    @Inject HSDSession mSession;
    @Inject LocationUpdateServiceManager mLocationUpdateManager;
    Context context;
    public static boolean isCaching;

    public SessionUtils(Context context){
        HSDInject.inject(this);
        this.context = context;
        isCaching = false;
    }

    public void cacheUserSession() {
        int loginType = HelperFunctions.getIntPreference("loginType", context);

        if(loginType == 1){
            //normal login
            String navPreference = HelperFunctions.getStringPreference(context.getResources().getString(R.string.navigationPreference),context.getApplicationContext());
            if(navPreference == null || navPreference.isEmpty()){
                HelperFunctions.setStringPreference(context.getResources().getString(R.string.navigationPreference),
                        context.getResources().getString(R.string.googleMapsPreference), context.getApplicationContext());
            }
            String userName = HelperFunctions.getStringPreference("userName", context);
            String password = HelperFunctions.getStringPreference("password", context);

            if(!TextUtils.isNullOrEmpty(userName) && !TextUtils.isNullOrEmpty(password))
                login(userName, password, context);
        }
    }

    private void login(String userName, String password, final Context context) {
        if (HelperFunctions.isNetworkAvailable(context)) {
            isCaching = true;
            String url = UrlBuilder.create(UrlBuilder.Endpoint.LOGIN).build();
            final Map<String, String> parameters = new HashMap<>();
            parameters.put("email", userName);
            parameters.put("password", password);

            Response.Listener<String> mLoginResponse = new Response.Listener<String>() {
                @Override public void onResponse(String response) {
                    LoginData loginData = LoginDataTranslator.getLoginData(response);
                    if (LoginDataValidator.isValid(loginData)) {
                        cacheUser(loginData);
                        requestUpcomingRides();
                    }
                }
            };

            Response.ErrorListener mLoginErrorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    HelperFunctions.showLongToast("Couldn't log in to our server.", context);
                }
            };

            StringRequest request = new StringRequest(Request.Method.POST, url, mLoginResponse, mLoginErrorListener){
                @Override protected Map<String, String> getParams() { return parameters; }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response){
                    HelperFunctions.setCookie(response.headers, context);
                    return super.parseNetworkResponse(response);
                }
            };

            mRequestQueue.add(request);
        }
    }

    private void cacheUser(LoginData loginData) {
        User user = mSession.getUser();
        if(UserValidator.isValid(user)) {
            user.setLoginData(loginData);
            if (user.getLoginData().getIsDriver())
                user.setCurrentRole(User.UserRole.DRIVER);
            if (user.getLoginData().getIsParent())
                user.setCurrentRole(User.UserRole.PARENT);
        }
    }

    private void requestUpcomingRides() {
        String url = UrlBuilder.create(UrlBuilder.Endpoint.RIDES).build() + "?limit=20";
        Response.Listener<String> mRidesResponse = new Response.Listener<String>() {
            @Override public void onResponse(String response) {
                List<Ride> rides = RidesTranslator.getRidesList(response);
                cacheUpcomingRides(rides);
                cacheCurrentRideToTrack(rides);
            }
        };

        Response.ErrorListener mRidesErrorListener = new Response.ErrorListener() {
            @Override public void onErrorResponse(VolleyError volleyError) {
                // TODO: Implement error handling
            }
        };

        StringRequest request = new StringRequest(Request.Method.GET, url, mRidesResponse, mRidesErrorListener) {
            @Override public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String cookie = HelperFunctions.getCookie(context);
                headers.put("Cookie", cookie);
                return headers;
            }
        };
        mRequestQueue.add(request);
    }

    private void cacheUpcomingRides(List<Ride> rides) {
        if (RidesValidator.isValid(rides))
            mSession.setUpcomingRides(rides);
    }

    private void cacheCurrentRideToTrack(List<Ride> rides) {
        for(int i = 0; i < rides.size(); i++)
            if(rides.get(i).isCheckedIn()) {
                mSession.setCurrentRide(rides.get(i));
                mLocationUpdateManager.startTracking(rides.get(i));
            }
    }
}
