package com.hopskipdrive.hsd_android_v2.util;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {

    private static final String REGEX_EMAIL = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        Pattern pattern = Pattern.compile(REGEX_EMAIL, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isNullOrEmpty(String input) {
        return input == null || input.length() == 0;
    }

    public static String convertCentsToDollars(int cents) {
        double dollars = ((double)cents) /100;
        return NumberFormat.getCurrencyInstance().format(dollars);
    }
}
