package com.hopskipdrive.hsd_android_v2.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.hopskipdrive.hsd_android_v2.activities.SplashActivity;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Eli on 5/11/2015.
 * This class provides us useful functions to use throughout our app.
 */
public class HelperFunctions {

    private static final String SET_COOKIE_KEY = "Set-Cookie";
    private static final String COOKIE_KEY = "Cookie";
    private static final String SESSION_COOKIE = "connect.sid";

    public static void setStringPreference(String label, String key, Context context) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(label, key)
                .commit();
    }

    public static String getStringPreference(String label, Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(label, "");
    }

    public static void setIntPreference(String label, int key, Context context) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putInt(label, key)
                .commit();
    }

    public static int getIntPreference(String label, Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getInt(label, 0);
    }

    public static void setCookie(Map<String, String> headers, Context context) {
        if (headers.containsKey(SET_COOKIE_KEY)
                && headers.get(SET_COOKIE_KEY).startsWith(SESSION_COOKIE))
        {
            String cookie = headers.get(SET_COOKIE_KEY);
            setStringPreference(COOKIE_KEY, cookie, context);
        }
    }

    public static String getCookie(Context context){
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(COOKIE_KEY, "");
    }

    //Creates a long toast for us with the appropriate message
    public static void showLongToast(String message, Context context){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    //Creates a short toast for us with the appropriate message
    public static void showShortToast(String message, Context context){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    //Checks if the device has network access
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
        else
            return false;
    }

    public static String[] getRegularTime(String militaryTime)
    {
        String[] regularTime;
        regularTime = militaryTime.trim().split(" ", 6);

        //Add   st, nd, rd, th   to day of the month
        if(regularTime[2]!= null) {
            int day;
            try {
                day = Integer.parseInt(regularTime[2]);
            }catch (NumberFormatException e){ day = 0; }

            if (regularTime[2].equals("11") || regularTime[2].equals("12") || regularTime[2].equals("13"))
                regularTime[2] += "th";
            else {
                switch ( day % 10) {
                    case 1:
                        regularTime[2] = day + "st";
                        break;
                    case 2:
                        regularTime[2] = day + "nd";
                        break;
                    case 3:
                        regularTime[2] = day + "rd";
                        break;
                    default:
                        regularTime[2] = day + "th";
                }
            }
        }

        //Convert military time to regular time
        if(regularTime[3] != null){
            int hour;
            String AMPM;
            String[] time = regularTime[3].trim().split(":", 3);

            //Get the hour of the drive
            try {
                hour = Integer.parseInt(time[0]);
            }catch (NumberFormatException e){ hour = 0; }

            //Convert military time to regular time
            if(hour >= 13){
                hour -= 12;
                AMPM = "PM";
            }else if(hour == 12)
                AMPM = "PM";
            else
                AMPM = "AM";

            //Set the regular time string
            regularTime[3] = hour + ":" + time[1] + AMPM;
        }
        return regularTime;
    }

    //Provides us the time away from a ride happening today
    public static String getTimeAwayString(String rideMonth, String rideDay, String rideTime)
    {
        String currentMonth, currentDay, currentTime, timeAway;
        final Calendar calendar = Calendar.getInstance();

        //current month
        SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM", Locale.ENGLISH);
        currentMonth = monthFormatter.format(calendar.getTime());

        //current day
        SimpleDateFormat dayFormatter = new SimpleDateFormat("dd", Locale.ENGLISH);
        currentDay = dayFormatter.format(calendar.getTime());

        //Check if the drive is for today
        if(rideMonth.equals(currentMonth) && rideDay.equals(currentDay)) {
            Date startDate = null, endDate = null;
            long duration, diffInMinutes, diffInHours;

            //Set the format we are going to compare times
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            currentTime = timeFormatter.format(calendar.getTime());

            try {
                startDate = timeFormatter.parse(currentTime);
                endDate   = timeFormatter.parse(rideTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Get the difference time between now and the drive time
            if(startDate != null && endDate != null) {
                duration = endDate.getTime() - startDate.getTime();
            }else
                duration = 0;

            //Get how many hours and minutes are remaining
            if(duration != 0) {
                diffInHours = TimeUnit.MILLISECONDS.toHours(duration);

                diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                if(diffInMinutes >=60){ diffInMinutes -= diffInHours*60; }

            }else
                return "";

            //Set the time away string
            if(diffInHours == 0){
                timeAway = diffInMinutes + " min";
            }else{
                if(diffInHours == 1 && diffInMinutes == 0)
                    timeAway = diffInHours + " hr";
                else if(diffInHours > 1 && diffInMinutes == 0)
                    timeAway = diffInHours + " hrs";
                else if(diffInHours == 1 && diffInMinutes < 60)
                    timeAway = diffInHours + " hr & " + diffInMinutes + " min";
                else
                    timeAway = diffInHours + " hrs & " + diffInMinutes + " min";
            }

            return timeAway;
        }else
            return "";
    }

    //gets the time of the drive in military time (hh:mm)
    public static String getDriveMilitaryTime(String driveDate) {
        String[] regularTime, time;
        regularTime = driveDate.trim().split(" ", 6);
        time = regularTime[3].trim().split(":", 3);
        return (time[0]+":"+time[1]);
    }

    public static String getDriveDay(String driveDate) {
        String[] regularTime;
        regularTime = driveDate.trim().split(" ", 6);
        int day;
        String dayString = "";
        //Add   st, nd, rd, th   to day of the month
        if(regularTime[2]!= null) {
            try {
                day = Integer.parseInt(regularTime[2]);
                dayString = String.valueOf(day);
            } catch (Throwable e){ e.printStackTrace(); }
        }
        return dayString;
    }

    public static String convertMilitaryTimeToRegularTime(String militaryTime){
        int hour;
        String minute;
        String AMPM;
        String[] time = militaryTime.split(":");

        //Get the hour of the drive
        if(time.length == 2) {
            try {
                hour = Integer.parseInt(time[0]);
            } catch (NumberFormatException e) {
                return "";
            }

            //Convert military time to regular time
            if (hour >= 13) {
                hour -= 12;
                AMPM = "PM";
            } else if (hour == 12)
                AMPM = "PM";
            else
                AMPM = "AM";

            if(time[1] != null && !time[1].isEmpty())
                minute = time[1];
            else
                return "";

            return (hour + ":" + minute + AMPM);
        }
        return "";
    }

    public static String getAgeFromDOB(String dob) {
        String age = "";
        try {
            String[] nums = dob.split("-");
            LocalDate birthdate = new LocalDate(Integer.valueOf(nums[0]), Integer.valueOf(nums[1]), Integer.valueOf(nums[2]));
            LocalDate now = new LocalDate();
            age = Years.yearsBetween(birthdate, now).toString();
            age = age.substring(1, age.length()-1); //remove 1st & last character
        }catch (Throwable e){
            e.printStackTrace();
        }
        return age;
    }

    public static String convertTSToRegularTime(Long timestamp) {
        Date date = new Date(timestamp);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateFormatted = formatter.format(date);
        return convertMilitaryTimeToRegularTime(dateFormatted);
    }

    public static void restartApp(Context context){
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static long[] getTimeAwayFromPickup(long diff) {
        long[] result = new long[5];
        final int ONE_DAY = 1000 * 60 * 60 * 24;
        final int ONE_HOUR = ONE_DAY / 24;
        final int ONE_MINUTE = ONE_HOUR / 60;
        final int ONE_SECOND = ONE_MINUTE / 60;

        //Day
        long d = diff / ONE_DAY;
        diff %= ONE_DAY;

        //Hours
        long h = diff / ONE_HOUR;
        diff %= ONE_HOUR;

        //Minutes
        long m = diff / ONE_MINUTE;
        diff %= ONE_MINUTE;

        //Seconds
        long s = diff / ONE_SECOND;
        long ms = diff % ONE_SECOND;
        result[0] = d;
        result[1] = h;
        result[2] = m;
        result[3] = s;
        result[4] = ms;

        return result;
    }
}
