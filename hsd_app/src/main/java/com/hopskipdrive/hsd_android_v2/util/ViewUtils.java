package com.hopskipdrive.hsd_android_v2.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.models.Rider;

public class ViewUtils {

    //Function that forces the native keyboard to hide
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager =
            (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void dismiss(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void addPassengerList(Activity activity, LinearLayout passengerLayout, Rider rider) {
        View passengerView = activity.getLayoutInflater().inflate(R.layout.passenger_list_row, null);
        TextView passengerNameText = (TextView) passengerView.findViewById(R.id.passenger_name);
        TextView passengerAgeText = (TextView) passengerView.findViewById(R.id.passenger_age);
        TextView passengerGenderText = (TextView) passengerView.findViewById(R.id.passenger_gender);
        TextView passengerBoosterSeatText = (TextView) passengerView.findViewById(R.id.passenger_booster_seat);
        TextView passengerSecretCodeText = (TextView) passengerView.findViewById(R.id.passenger_secret_code);
        TextView passengerSpecialInstructionsText = (TextView) passengerView.findViewById(R.id.passenger_special_instructions);

        //Rider Name
        passengerNameText.setText(rider.getName());

        //Rider Age
        String age = HelperFunctions.getAgeFromDOB(rider.getDOB());
        passengerAgeText.setText(age);

        //Rider Gender
        if(rider.getGender().equals("m"))
            passengerGenderText.setText("male");
        else
            passengerGenderText.setText("female");

        //Rider Password
        passengerSecretCodeText.setText(rider.getPassword());

        //Rider Notes
        if(rider.getNotes().isEmpty())
            passengerSpecialInstructionsText.setText("None");
        else
            passengerSpecialInstructionsText.setText(rider.getNotes());

        //Rider Booster seat needed
        if(rider.getBoosterSeatStatus() || Integer.valueOf(age) <= 7) {
            passengerBoosterSeatText.setText("Yes");
            passengerBoosterSeatText.setTextColor(activity.getResources().getColor(R.color.hsd_blue));
        }else{ passengerBoosterSeatText.setText("No"); }

        passengerLayout.addView(passengerView);
    }

    public static void addShortPassengerList(Activity activity, LinearLayout passengerLayout, Rider rider, int riderNumber) {
        View passengerView = activity.getLayoutInflater().inflate(R.layout.short_passenger_list_row, null);
        TextView passengerNameText = (TextView) passengerView.findViewById(R.id.passenger_number);
        TextView passengerBoosterSeatText = (TextView) passengerView.findViewById(R.id.passenger_booster_seat);
        TextView passengerSpecialInstructionsText = (TextView) passengerView.findViewById(R.id.passenger_special_instructions);

        //Rider Number
        passengerNameText.setText("Rider #" + riderNumber);

        //Rider Booster seat needed
        String age = HelperFunctions.getAgeFromDOB(rider.getDOB());
        if(rider.getBoosterSeatStatus() || Integer.valueOf(age) <= 7) {
            passengerBoosterSeatText.setText("Yes");
            passengerBoosterSeatText.setTextColor(activity.getResources().getColor(R.color.hsd_blue));
        }else{ passengerBoosterSeatText.setText("No"); }

        //Rider Notes
        if(rider.getNotes().isEmpty())
            passengerSpecialInstructionsText.setText("None");
        else
            passengerSpecialInstructionsText.setText(rider.getNotes());

        passengerLayout.addView(passengerView);
    }

    public static void openAppForDirections(Context context, boolean isGoogleMap, String destinationLat, String destinationLng) {
        try {
            if (isGoogleMap) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + destinationLat + "," + destinationLng));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                context.startActivity(intent);
            } else {
                String url = "waze://?ll=" + destinationLat + "," + destinationLng + "&navigate=yes";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(intent);
            }
        } catch (ActivityNotFoundException e) { e.printStackTrace(); }
    }

    //Open the web with a specific URL
    public static void openURL(Context context, String url) {
        Intent iIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(iIntent);
    }
}
