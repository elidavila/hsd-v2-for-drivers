package com.hopskipdrive.hsd_android_v2.util;

import android.widget.TextView;

public class ViewHelper {

    public static void setText(TextView textView, String text) {
        setText(textView, text, null);
    }

    public static void setText(TextView textView, String text, String textIfNull) {
        if (!TextUtils.isNullOrEmpty(text)) {
            textView.setText(text);
        }
        else if (!TextUtils.isNullOrEmpty(textIfNull)) {
            textView.setText(textIfNull);
        }
    }
}
