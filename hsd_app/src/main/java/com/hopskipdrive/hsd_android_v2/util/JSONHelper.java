package com.hopskipdrive.hsd_android_v2.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONHelper {
    public static String getString(JSONObject obj, String key) {
        String val = null;
        if (containsKey(obj, key)) {
            try {
                val = obj.getString(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static long getLong(JSONObject obj, String key) {
        long val = 0;
        if (containsKey(obj, key)) {
            try {
                val = obj.getLong(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static double getDouble(JSONObject obj, String key) {
        double val = 0;
        if (containsKey(obj, key)) {
            try {
                val = obj.getDouble(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static boolean getBoolean(JSONObject obj, String key) {
        boolean val = false;
        if (containsKey(obj, key)) {
            try {
                val = obj.getBoolean(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static JSONObject getJSONObject(JSONObject obj, String key) {
        JSONObject val = null;
        if (containsKey(obj, key)) {
            try {
                val = obj.getJSONObject(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static int getInt(JSONObject obj, String key) {
        int val = 0;
        if (containsKey(obj, key)) {
            try {
                val = obj.getInt(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static JSONArray getJSONArray(JSONObject obj, String key) {
        JSONArray val = null;
        if (containsKey(obj, key)) {
            try {
                val = obj.getJSONArray(key);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static boolean containsKey(JSONObject obj, String key) {
        return obj != null && obj.has(key);
    }

    public static void put(JSONObject obj, String key, String value) {
        if (obj != null && !TextUtils.isNullOrEmpty(key) && !TextUtils.isNullOrEmpty(value)) {
            try {
                obj.put(key, value);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void put(JSONObject obj, String key, double value) {
        if (obj != null && !TextUtils.isNullOrEmpty(key)) {
            try {
                obj.put(key, value);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
