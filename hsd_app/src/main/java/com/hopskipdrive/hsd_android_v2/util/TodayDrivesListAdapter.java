package com.hopskipdrive.hsd_android_v2.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.hopskipdrive.hsd_android_v2.R;

import java.util.ArrayList;

/**
 * Created by Eli on 5/18/2015.
 * This adapter sets the drives list in
 * today's Rides screen for drivers
 */
public class TodayDrivesListAdapter extends ArrayAdapter<String>{

    Activity activity;
    String[] driveTimes;
    ArrayList<String> driveDateList, driveNameList;
    String driveDayString, militaryDriveTime, timeAwayString;
    TextView driveDate, driveName, timeAwayTextView;
    View rowView;
    LayoutInflater inflater;

    //Constructor to initialize our variables
    public TodayDrivesListAdapter(Activity activity,
                                  ArrayList<String> driveDateList,
                                  ArrayList<String> driveNameList){
        super(activity, R.layout.upcoming_drives_list, driveDateList);
        this.activity=activity;
        this.driveDateList = driveDateList;
        this.driveNameList = driveNameList;
    }

    public View getView(int position, View view, ViewGroup parent) {
        //Set layout view for list
        inflater = activity.getLayoutInflater();
        rowView = inflater.inflate(R.layout.upcoming_drives_list, null, true);

        //Get View items
        driveDate = (TextView) rowView.findViewById(R.id.driveDateTextView);
        driveName = (TextView) rowView.findViewById(R.id.driveNameTextView);
        timeAwayTextView = (TextView) rowView.findViewById(R.id.timeAwayTextView);

        //Get the pick up time in each ride
        if(driveDateList != null) {
            driveTimes = HelperFunctions.getRegularTime(driveDateList.get(position));
            driveDayString = HelperFunctions.getDriveDay(driveDateList.get(position));
            militaryDriveTime = HelperFunctions.getDriveMilitaryTime(driveDateList.get(position));

            //Set the drive pickup date & the away time(if applicable)
            if (driveTimes != null) {
                timeAwayString = HelperFunctions.getTimeAwayString(driveTimes[1], driveDayString, militaryDriveTime);
                if (!timeAwayString.isEmpty()) {
                    driveDate.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3] + " |");

                    //Check if the pick up time has not passed yet.
                    if(!timeAwayString.contains("-"))
                        timeAwayTextView.setText(timeAwayString + " away");
                    else
                        timeAwayTextView.setText("In Progress");

                    timeAwayTextView.setVisibility(View.VISIBLE);
                } else {
                    driveDate.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
                    timeAwayTextView.setVisibility(View.INVISIBLE);
                }
            }
        }

        //Set the drive name
        driveName.setText(driveNameList.get(position));

        return rowView;

    }

}