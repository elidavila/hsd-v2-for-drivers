package com.hopskipdrive.hsd_android_v2.util;

import java.util.Collections;
import java.util.Map;

public class UrlBuilder {

    private static final Scheme DEFAULT_SCHEME = Scheme.HTTPS;
    private static final Host DEFAULT_HOST = Host.BETA;
    private static final Prefix DEFAULT_PREFIX = Prefix.API_V2;
    private Object[] mArgs = null;
    private Map<String, String> mParams = Collections.emptyMap();

    public enum Scheme {
        HTTP ("http://"),
        HTTPS ("https://");

        private final String mScheme;

        Scheme(String scheme) {
            mScheme = scheme;
        }

        public String getScheme() {
            return mScheme;
        }
    }

    public enum Host {
        TEST ("test.hopskipdev.com/"),
        STAGE ("stage.hopskipdev.com/"),
        BETA ("beta.hopskipdrive.com/");

        private final String mHost;

        Host(String host) {
            mHost = host;
        }

        public String getHost() {
            return mHost;
        }
    }

    public enum Prefix {
        API_V2("api/v2/");

        private final String mPrefix;

        Prefix(String prefix) {
            mPrefix = prefix;
        }

        public String getPrefix() {
            return mPrefix;
        }
    }

    public enum Endpoint {
        RIDE("ride/%s"),
        RIDES ("rides"),
        RIDES_COMPLETED ("rides/completed"),
        POSITION ("position"),
        LOGIN ("login"),
        DEVICE ("device");

        private final String mPath;

        Endpoint(String path) {
            mPath = path;
        }

        public String getPath() {
            return mPath;
        }
    }

    private Scheme mScheme = DEFAULT_SCHEME;
    private Host mHost = DEFAULT_HOST;
    private Prefix mPrefix = DEFAULT_PREFIX;
    private Endpoint mEndpoint;

    public static UrlBuilder create(Endpoint endpoint) {
        UrlBuilder builder = new UrlBuilder();
        builder.mEndpoint = endpoint;
        return builder;
    }

    public UrlBuilder withArgs(Object... args) {
        mArgs = args;
        return this;
    }

    public UrlBuilder withParams(Map<String, String> params) {
        mParams = params;
        return this;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append(mScheme.getScheme());
        sb.append(mHost.getHost());
        sb.append(mPrefix.getPrefix());
        sb.append(String.format(mEndpoint.getPath(), mArgs));
        if (mParams.size() > 0) {
            sb.append("?");
            for (String key : mParams.keySet()) {
                sb.append(key);
                sb.append("=");
                sb.append(mParams.get(key));
                sb.append("&");
            }
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
