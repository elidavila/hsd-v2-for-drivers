package com.hopskipdrive.hsd_android_v2.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

/**
 * Created by Eli on 5/18/2015.
 * This adapter sets the rides list in
 * today's Rides screen for parents.
 */
public class TodayRidesListAdapter extends ArrayAdapter<String>{

    int rounded_value = 120;
    Activity activity;
    ArrayList<String> rideDateList, rideNameList, driverPicList;
    private ImageLoader imageLoader;
    DisplayImageOptions options;
    LayoutInflater inflater;
    View rowView;
    ImageView driverImage;
    TextView rideDate, rideName;    
    String[] rideTimes;
    
    public TodayRidesListAdapter(Activity activity,
                                 ArrayList<String> rideDateList,
                                 ArrayList<String> rideNameList,
                                 ArrayList<String> driverPicList) {
        super(activity, R.layout.upcoming_rides_list, rideDateList);
        this.activity=activity;
        this.rideDateList = rideDateList;
        this.rideNameList = rideNameList;
        this.driverPicList = driverPicList;

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).cacheOnDisc(true)
                .displayer(new RoundedBitmapDisplayer(rounded_value))
                .build();
        imageLoader= ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(activity));

    }

    public View getView(int position, View view, ViewGroup parent) {
        //Set layout view for list
        inflater = activity.getLayoutInflater();
        rowView = inflater.inflate(R.layout.upcoming_rides_list, null, true);

        //Get View items
        driverImage = (ImageView) rowView.findViewById(R.id.driverImageView);
        rideDate = (TextView) rowView.findViewById(R.id.rideDateTextView);
        rideName = (TextView) rowView.findViewById(R.id.rideNameTextView);

        //Set the rides listview with ride date, ride name, and driver Picture
        rideTimes = HelperFunctions.getRegularTime(rideDateList.get(position));
        rideDate.setText(rideTimes[0] + " " + rideTimes[1] + " " + rideTimes[2] + " at " + rideTimes[3]);

        rideName.setText(rideNameList.get(position));

        //Check if the driver has a url, if not then add empty driver
        if(driverPicList.get(position) == null || driverPicList.get(position).isEmpty())
            driverImage.setImageResource(R.mipmap.user2x);
        else
            imageLoader.displayImage(driverPicList.get(position), driverImage, options);

        return rowView;

    }

}