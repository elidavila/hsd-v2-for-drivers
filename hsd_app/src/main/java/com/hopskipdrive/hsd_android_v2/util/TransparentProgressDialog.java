package com.hopskipdrive.hsd_android_v2.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hopskipdrive.hsd_android_v2.R;

/**
 * Created by Eli on 5/12/2015.
 * This class provides a custom loading lock. This lock prevents the application
 * to perform further action until we disable the lock. Common use of this class will be
 * when we call HTTP Methods and wait for the response.
 */

public class TransparentProgressDialog extends Dialog {
    TextView t;

    public TransparentProgressDialog(Context context) {

        super(context, R.style.TransparentProgressDialog);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.setContentView(R.layout.custom_progress_dialog);
        this.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        setCanceledOnTouchOutside(false);
        this.setCancelable(false);

        t = (TextView) this.findViewById(R.id.textView1);
        t.setVisibility(View.GONE);

    }

    @Override
    public void show() {
        super.show();
    }

    public void setMessage(String s) {
        t.setVisibility(View.VISIBLE);
        t.setText(s);

    }
}
