package com.hopskipdrive.hsd_android_v2.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class GCMUtils {
    public static final String REG_ID_KEY = "Reg.Id";
    public static final String USERNAME_REG_ID_KEY = "Username.Reg.Id";
    public static final String APP_VERSION_KEY = "App.Version";

    public static String getRegistrationId(Context context) {

        String registrationId = HelperFunctions.getStringPreference(REG_ID_KEY, context);
        if (registrationId.isEmpty()) return "";

        int registeredVersion = HelperFunctions.getIntPreference(APP_VERSION_KEY, context);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) return "";

        return registrationId;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public static String getPreviousRegistrationId(Context context){
        return HelperFunctions.getStringPreference(USERNAME_REG_ID_KEY, context);
    }
}
