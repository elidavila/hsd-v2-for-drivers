package com.hopskipdrive.hsd_android_v2.models;

import java.util.List;

public class Rides {
    private List<Ride> upcomingRides;
    private List<Ride> completedRides;

    public void setUpcomingRides(List<Ride> upcomingRides) { this.upcomingRides = upcomingRides; }
    public List<Ride> getUpcomingRides() {
        return upcomingRides;
    }

    public void setCompletedRides(List<Ride> completedRides) { this.completedRides = completedRides; }
    public List<Ride> getCompletedRides() {
        return completedRides;
    }
}
