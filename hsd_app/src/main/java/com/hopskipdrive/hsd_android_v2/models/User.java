package com.hopskipdrive.hsd_android_v2.models;

import com.hopskipdrive.hsd_android_v2.models.validators.LoginDataValidator;

/**
 * Created by Eli on 5/13/2015.
 * This class represents the user data model.
 * Whenever we need to grab info about the current
 * user, we refer to here.
 */
public class User {
    private UserRole currentRole;

    private boolean isDriverInShift = false;
    private LoginData loginData;

    public enum UserRole {
        DRIVER, PARENT, BOTH;
    }

    public boolean isDriverInShift() {
        return isDriverInShift;
    }

    public void setDriverInShift(boolean isDriverInShift) {
        this.isDriverInShift = isDriverInShift;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
        if (LoginDataValidator.isValid(loginData)) {

        }
    }

    public UserRole getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(UserRole currentRole) {
        this.currentRole = currentRole;
    }
}
