package com.hopskipdrive.hsd_android_v2.models.translators;

import android.location.Location;
import com.hopskipdrive.hsd_android_v2.models.LocationUpdate;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class LocationUpdateTranslator {

    public static JSONObject getUpdateParams(LocationUpdate update) {
        JSONObject obj = new JSONObject();
        JSONHelper.put(obj, LocationUpdate.Keys.RIDE_ID, update.getRide().get_id());
        JSONHelper.put(obj, LocationUpdate.Keys.SHIFT_ID, update.getRide().getShiftId());
        JSONHelper.put(obj, LocationUpdate.Keys.LATITUDE, update.getLocation().getLatitude());
        JSONHelper.put(obj, LocationUpdate.Keys.LONGITUDE, update.getLocation().getLongitude());
        JSONHelper.put(obj, LocationUpdate.Keys.TIME, update.getLocation().getTime());
        JSONHelper.put(obj, LocationUpdate.Keys.SPEED, update.getSpeed());
        return obj;
    }

    public static JSONArray getUpdatesArray(Ride ride, Collection<Location> locations) {
        JSONArray arr = new JSONArray();
        for (Location location : locations) {
            JSONObject obj = new JSONObject();
            JSONHelper.put(obj, LocationUpdate.Keys.RIDE_ID, ride.get_id());
            JSONHelper.put(obj, LocationUpdate.Keys.SHIFT_ID, ride.getShiftId());
            JSONHelper.put(obj, LocationUpdate.Keys.LATITUDE, location.getLatitude());
            JSONHelper.put(obj, LocationUpdate.Keys.LONGITUDE, location.getLongitude());
            JSONHelper.put(obj, LocationUpdate.Keys.TIME, location.getTime());
            JSONHelper.put(obj, LocationUpdate.Keys.SPEED, location.getSpeed() * LocationUpdate.MS_TO_MPH);
            arr.put(obj);
        }
        return arr;
    }

    public static Map<String, String> getUpdatesArrayQueryParams(Ride ride) {
        Map<String, String> params = new HashMap<>();
        params.put(LocationUpdate.Keys.RIDE_ID, ride.get_id());
        params.put(LocationUpdate.Keys.SHIFT_ID, ride.getShiftId());
        return params;
    }
}
