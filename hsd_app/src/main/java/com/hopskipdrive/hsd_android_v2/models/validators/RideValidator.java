package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;

public class RideValidator {

    public static boolean isValid(Ride ride) {
        // TODO: Determine what is the base requirement for a "valid" ride
        return
            ride != null &&
            !TextUtils.isNullOrEmpty(ride.get_id());
    }
}
