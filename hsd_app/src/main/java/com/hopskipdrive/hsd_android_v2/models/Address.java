package com.hopskipdrive.hsd_android_v2.models;

import java.io.Serializable;

public class Address implements Serializable {
    private String zip;

    private String phone;

    private String addr1;

    private String _id;

    private String name;

    private int __v;

    private String state;

    private String parent;

    private double lng;

    private double lat;

    private String city;

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static class Keys {
        public static final String ID = "_id";
        public static final String PARENT = "parent";
        public static final String NAME = "name";
        public static final String ADDR1 = "addr1";
        public static final String CITY = "city";
        public static final String STATE = "state";
        public static final String ZIP = "zip";
        public static final String PHONE = "phone";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String V = "__v";
    }
}
