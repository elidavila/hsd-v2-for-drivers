package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.Driver;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;

public class DriverValidator {
    public static boolean isValid(Driver driver) {
        return driver != null && !TextUtils.isNullOrEmpty(driver.get_id());
    }

    public static boolean hasPhoto(Driver driver) {
        return isValid(driver) && !TextUtils.isNullOrEmpty(driver.getPhoto_url());
    }
}
