package com.hopskipdrive.hsd_android_v2.models.translators;

import com.hopskipdrive.hsd_android_v2.models.Driver;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONObject;

public class DriverTranslator {
    public static Driver getDriver(JSONObject obj) {
        Driver driver = new Driver();
        driver.set_id(JSONHelper.getString(obj, Driver.Keys.ID));
        driver.setName(JSONHelper.getString(obj, Driver.Keys.NAME));
        driver.setInfo_url(JSONHelper.getString(obj, Driver.Keys.INFO_URL));
        driver.setPhoto_url(JSONHelper.getString(obj, Driver.Keys.PHOTO_URL));
        driver.setSince(JSONHelper.getString(obj, Driver.Keys.SINCE));
        driver.setInfo(JSONHelper.getString(obj, Driver.Keys.NOTES));
        return driver;
    }
}
