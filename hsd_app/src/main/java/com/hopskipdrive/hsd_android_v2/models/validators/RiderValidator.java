package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;

import java.util.List;

public class RiderValidator {
    public static boolean isValid(List<Rider> riders) {
        return riders != null && riders.size() > 0;
    }

    public static boolean isValid(Rider rider) {
        return rider != null && !TextUtils.isNullOrEmpty(rider.getName());
    }
}
