package com.hopskipdrive.hsd_android_v2.models;

import android.location.Location;

public class LocationUpdate {
    public static final double MS_TO_MPH = 2.23694;

    Ride mRide;
    Location mLocation;

    public LocationUpdate(Ride ride, Location location) {
        mRide = ride;
        mLocation = location;
    }

    public Ride getRide() {
        return mRide;
    }

    public Location getLocation() {
        return mLocation;
    }

    public double getSpeed() {
        return mLocation.getSpeed() * MS_TO_MPH;
    }

    public static class Keys {
        public static final String RIDE_ID = "rideId";
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lng";
        public static final String TIME = "time";
        public static final String SPEED = "speed";
        public static final String SHIFT_ID = "shiftId";
    }
}
