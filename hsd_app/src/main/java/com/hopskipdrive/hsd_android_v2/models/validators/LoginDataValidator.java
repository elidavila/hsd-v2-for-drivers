package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.LoginData;

public class LoginDataValidator {
    public static boolean isValid(LoginData loginData) {
        return loginData != null && loginData.getOk();
    }
}
