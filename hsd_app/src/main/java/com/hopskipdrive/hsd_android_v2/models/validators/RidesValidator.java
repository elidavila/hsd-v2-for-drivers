package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.Ride;

import java.util.List;

public class RidesValidator {
    public static boolean isValid(List<Ride> rides) {
        return rides != null && !rides.isEmpty();
    }

    public static boolean isEmpty(List<Ride> rides) {
        return rides != null && rides.isEmpty();
    }
}
