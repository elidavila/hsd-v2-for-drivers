package com.hopskipdrive.hsd_android_v2.models.translators;

import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.List;

public class PhoneNumbersTranslator {
    public static LinkedHashMap<String, String> getPhoneNumbers(JSONObject phoneNumbersObj, String parentId, List<Rider> riders) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        try{
            //Get parent phone number
            if(phoneNumbersObj.has(parentId))
                map.put("Parent", phoneNumbersObj.getString(parentId));

            //Get each rider's phone number
            if (RiderValidator.isValid(riders))
                for(int i = 0; i < riders.size(); i++)
                    if(phoneNumbersObj.has(riders.get(i).get_id()))
                        map.put(riders.get(i).getName().split(" ")[0], phoneNumbersObj.getString(riders.get(i).get_id()));

        }catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }

}
