package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.util.TextUtils;

public class AddressValidator {
    public static boolean isValid(Address address) {
        return address != null && !TextUtils.isNullOrEmpty(address.get_id());
    }
}
