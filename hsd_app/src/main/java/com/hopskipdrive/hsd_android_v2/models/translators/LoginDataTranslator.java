package com.hopskipdrive.hsd_android_v2.models.translators;

import com.crashlytics.android.Crashlytics;
import com.hopskipdrive.hsd_android_v2.models.LoginData;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONObject;

public class LoginDataTranslator {
    public static LoginData getLoginData(String json) {
        LoginData loginData = new LoginData();
        try{
            JSONObject obj = new JSONObject(json);
            if (JSONHelper.containsKey(obj, LoginData.Keys.ERR)) {
                loginData.setOk(false);
                loginData.setErrorMsg(JSONHelper.getString(obj, LoginData.Keys.ERR));
            }
            else {
                loginData.setOk(true);
            }
            loginData.setIsDriver(JSONHelper.getBoolean(obj, LoginData.Keys.DRIVER));
            loginData.setIsParent(JSONHelper.getBoolean(obj, LoginData.Keys.PARENT));
            loginData.setFirstName(JSONHelper.getString(obj, LoginData.Keys.FIRST));
        }
        catch (Exception e){
            Crashlytics.logException(e);
        }
        return loginData;
    }
}
