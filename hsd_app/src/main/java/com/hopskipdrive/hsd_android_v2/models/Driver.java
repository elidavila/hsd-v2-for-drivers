package com.hopskipdrive.hsd_android_v2.models;

import java.io.Serializable;
import java.util.List;

public class Driver implements Serializable {

	private String info;

	private String phone;

	private String last;

	private String hashed_password;

	private int __v;

	private String provider;

	private String billing;

	private long tos;

	private int numRiders;

	private String _id;

	private String email, since;

	private List<Cars> cars;

	private String verified;

	private String dob;

	private String driver_status;

	private String ui_mode;

	private String license;

	private String first;

	private String salt;

	private String name;

	private String info_url;

	private String photo_url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo_url() {
		return info_url;
	}

	public void setInfo_url(String info_url) {
		this.info_url = info_url;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getHashed_password() {
		return hashed_password;
	}

	public void setHashed_password(String hashed_password) {
		this.hashed_password = hashed_password;
	}

	public int get__v() {
		return __v;
	}

	public void set__v(int __v) {
		this.__v = __v;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getBilling() {
		return billing;
	}

	public void setBilling(String billing) {
		this.billing = billing;
	}

	public long getTos() {
		return tos;
	}

	public void setTos(long tos) {
		this.tos = tos;
	}

	public int getNumRiders() {
		return numRiders;
	}

	public void setNumRiders(int numRiders) {
		this.numRiders = numRiders;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Cars> getCars() {
		return cars;
	}

	public void setCars(List<Cars> cars) {
		this.cars = cars;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDriver_status() {
		return driver_status;
	}

	public void setDriver_status(String driver_status) {
		this.driver_status = driver_status;
	}

	public String getUi_mode() {
		return ui_mode;
	}

	public void setUi_mode(String ui_mode) {
		this.ui_mode = ui_mode;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSince() {
		return since;
	}

	public void setSince(String since) {
		this.since = since;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public static class Keys {

        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String INFO_URL = "info_url";
        public static final String PHOTO_URL = "photo_url";
        public static final String SINCE = "since";
        public static final String NOTES = "notes";
    }
}
