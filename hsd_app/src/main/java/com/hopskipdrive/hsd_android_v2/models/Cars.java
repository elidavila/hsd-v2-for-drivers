package com.hopskipdrive.hsd_android_v2.models;

import java.io.Serializable;

public class Cars implements Serializable {
	private String model;

	private String _id;

	private String color;

	private int year;

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getBoosterSeats() {
		return boosterSeats;
	}

	public void setBoosterSeats(int boosterSeats) {
		this.boosterSeats = boosterSeats;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public int getRiderSeats() {
		return riderSeats;
	}

	public void setRiderSeats(int riderSeats) {
		this.riderSeats = riderSeats;
	}

	private int boosterSeats;

	private String make;

	private int riderSeats;
}
