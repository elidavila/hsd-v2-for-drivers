package com.hopskipdrive.hsd_android_v2.models;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class Ride implements Serializable, Comparable<Ride> {

    public static final String STATUS_MATCHED = "matched";
    public static final String STATUS_CHECKIN = "checkin";
    public static final String STATUS_IN_PROGRESS = "inprogress";
    public static final String STATUS_DELIVERED = "delivered";
    public static final String STATUS_ARRIVED = "arrived";
    public static final String STATUS_PICKUP = "pickup";
    private Long actual_dropoff_ts;
    private Long actual_pickup_ts;

    public enum Status {
        CHECKIN (STATUS_CHECKIN),
        DELIVERED (STATUS_DELIVERED),
        ARRIVED (STATUS_ARRIVED),
        PICKUP (STATUS_PICKUP);

        private final String mName;

        Status(String name) {
            mName = name;
        }

        public String getName() {
            return mName;
        }
    }
    private LinkedHashMap<String, String> phone_numbers;
    private int driver_fee;
    private String actual_time;
    private Double actual_distance;

    private Address startingAddress;
    private String pickup;

    private String status;

    private int __v;

    private String parent;

    private Address destinationAddress;

    private String date;

    private String dropoff;

    private long dropoff_ts;

    private List<Rider> riders;

    private long pickup_ts;

    private String _id;

    private String name;

    private boolean escort2;

    private String driving_duration;

    private boolean escort1;

    private String notes, pickUpNotes, dropOffNotes;

    private String driving_summary;

    private String driving_distance;
    Driver driver;

    private Date pickUpDate;

    private String shiftId;

    private boolean checkedIn;

    public int getDriver_fee() { return driver_fee; }
    public void setDriver_fee(int driver_fee){ this.driver_fee = driver_fee; }

    public String getActual_time() { return actual_time; }
    public void setActual_time(String actual_time){ this.actual_time = actual_time; }

    public Double getActual_distance() { return actual_distance; }
    public void setActual_distance(Double actual_distance){ this.actual_distance = actual_distance; }

    public Long getActual_dropoff_ts() { return actual_dropoff_ts; }
    public void setActual_dropoff_ts(Long actual_dropoff_ts) { this.actual_dropoff_ts = actual_dropoff_ts; }

    public Long getActual_pickup_ts() { return actual_pickup_ts; }
    public void setActual_pickup_ts(Long actual_pickup_ts) { this.actual_pickup_ts = actual_pickup_ts; }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Address getSaddr() {
        return startingAddress;
    }

    public void setSaddr(Address startingAddress) {
        this.startingAddress = startingAddress;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Address getDaddr() {
        return destinationAddress;
    }

    public void setDaddr(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public LinkedHashMap<String, String> getPhone_numbers() {
        return phone_numbers;
    }

    public void setPhone_numbers(LinkedHashMap<String, String> phone_numbers) {
        this.phone_numbers = phone_numbers;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDropoff() {
        return dropoff;
    }

    public void setDropoff(String dropoff) {
        this.dropoff = dropoff;
    }

    public long getDropoff_ts() {
        return dropoff_ts;
    }

    public void setDropoff_ts(long dropoff_ts) {
        this.dropoff_ts = dropoff_ts;
    }

    public List<Rider> getRiders() {
        return riders;
    }

    public void setRiders(List<Rider> riders) {
        this.riders = riders;
    }

    public long getPickup_ts() {
        return pickup_ts;
    }

    public void setPickup_ts(long pickup_ts) {
        setPickUpDate(new Date(pickup_ts));
        this.pickup_ts = pickup_ts;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getEscort2() {
        return escort2;
    }

    public void setEscort2(boolean escort2) {
        this.escort2 = escort2;
    }

    public String getDriving_duration() {
        return driving_duration;
    }

    public void setDriving_duration(String driving_duration) {
        this.driving_duration = driving_duration;
    }

    public boolean getEscort1() {
        return escort1;
    }

    public void setEscort1(boolean escort1) {
        this.escort1 = escort1;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDriving_summary() {
        return driving_summary;
    }

    public void setDriving_summary(String driving_summary) {
        this.driving_summary = driving_summary;
    }

    public String getDriving_distance() {
        return driving_distance;
    }

    public void setDriving_distance(String driving_distance) {
        this.driving_distance = driving_distance;
    }


    public String getPickUpNotes() {
        return pickUpNotes;
    }

    public void setPickUpNotes(String pickUpNotes) {
        this.pickUpNotes = pickUpNotes;
    }

    public String getDropOffNotes() {
        return dropOffNotes;
    }

    public void setDropOffNotes(String dropOffNotes) {
        this.dropOffNotes = dropOffNotes;
    }

    public void setCheckedIn(boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public boolean isCheckedIn() {
        return checkedIn;
    }

    @Override
    public int compareTo(Ride ride) {

        return pickUpDate.compareTo(ride.getPickUpDate());
    }

    public Date getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(Date pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public static class Keys {
        public static final String ID = "_id";
        public static final String DRIVING_SUMMARY = "driving_summary";
        public static final String DRIVING_DURATION = "driving_duration";
        public static final String DRIVING_DISTANCE = "driving_distance";
        public static final String DROPOFF_TS = "dropoff_ts";
        public static final String PICKUP_TS = "pickup_ts";
        public static final String STATUS = "status";
        public static final String PARENT = "parent";
        public static final String NAME = "name";
        public static final String DATE = "date";
        public static final String PICKUP = "pickup";
        public static final String ESCORT1 = "escort1";
        public static final String ESCORT2 = "escort2";
        public static final String STARTING_ADDRESS = "saddr";
        public static final String DESTINATION_ADDRESS = "daddr";
        public static final String DROPOFF = "dropoff";
        public static final String NOTES = "notes";
        public static final String V = "__v";
        public static final String DROPOFF_NOTES = "dropoff_notes";
        public static final String PICKUP_NOTES = "pickup_notes";
        public static final String RIDERS = "riders";
        public static final String DRIVER = "driver";
        public static final String SHIFT_ID = "shiftId";
        public static final String DRIVER_FEE = "driver_fee";
        public static final String ACTUAL_TIME = "actual_time";
        public static final String ACTUAL_DISTANCE = "actual_distance";
        public static final String PHONE_NUMBERS = "phone_numbers";
        public static final String CHECKED_IN = "checkedIn";
        public static final String ACTUAL_PICKUP_TS = "actual_pickup_ts";
        public static final String ACTUAL_DROPOFF_TS = "actual_dropoff_ts";
    }
}
