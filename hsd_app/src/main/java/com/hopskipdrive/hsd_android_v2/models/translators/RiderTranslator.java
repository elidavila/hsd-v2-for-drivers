package com.hopskipdrive.hsd_android_v2.models.translators;

import com.crashlytics.android.Crashlytics;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RiderTranslator {
    public static List<Rider> getRiders(JSONArray ridersArray) {
        List<Rider> riders = new ArrayList<>();
        try {
            for(int i = 0; i < ridersArray.length(); i++) {
                JSONObject obj = ridersArray.getJSONObject(i);
                Rider rider = getRider(obj);
                if (RiderValidator.isValid(rider)) {
                    riders.add(rider);
                }
            }
        }
        catch (Exception e) {
            Crashlytics.logException(e);
        }
        return riders;
    }

    private static Rider getRider(JSONObject obj) {
        Rider rider = new Rider();
        rider.set_id(JSONHelper.getString(obj, Rider.Keys.ID));
        rider.setName(JSONHelper.getString(obj, Rider.Keys.NAME));
        rider.setPassword(JSONHelper.getString(obj, Rider.Keys.PASSWORD));
        rider.setAge(JSONHelper.getInt(obj, Rider.Keys.AGE));
        rider.setDOB(JSONHelper.getString(obj, Rider.Keys.DOB));
        rider.setGender(JSONHelper.getString(obj, Rider.Keys.GENDER));
        rider.setNotes(JSONHelper.getString(obj, Rider.Keys.NOTES));
        rider.setBoosterSeatStatus(JSONHelper.getBoolean(obj, Rider.Keys.BOOSTER));
        return rider;
    }
}
