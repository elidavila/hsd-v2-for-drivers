package com.hopskipdrive.hsd_android_v2.models;

public class Rider {
	String name, password, gender, notes, dob;
	int age;
    boolean boosterSeatStatus;
    private String _id;

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

    public String getDOB() { return dob; }
    public void setDOB(String dob) { this.dob = dob; }

    public String getGender() { return gender; }
    public void setGender(String gender) { this.gender = gender; }

    public boolean getBoosterSeatStatus() { return boosterSeatStatus; }
    public void setBoosterSeatStatus(boolean boosterSeatStatus) { this.boosterSeatStatus = boosterSeatStatus; }

    public String getNotes() { return notes; }
    public void setNotes(String notes) { this.notes = notes; }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }
    public static class Keys {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String PASSWORD = "password";
        public static final String AGE = "age";
        public static final String DOB = "dob";
        public static final String GENDER = "gender";
        public static final String BOOSTER = "booster";
        public static final String NOTES = "notes";
    }
}
