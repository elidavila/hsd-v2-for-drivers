package com.hopskipdrive.hsd_android_v2.models.translators;

import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONObject;

public class AddressTranslator {
    public static Address getAddress(JSONObject obj) {
        Address address = new Address();
        address.set_id(JSONHelper.getString(obj, Address.Keys.ID));
        address.setParent(JSONHelper.getString(obj, Address.Keys.PARENT));
        address.setName(JSONHelper.getString(obj, Address.Keys.NAME));
        address.setAddr1(JSONHelper.getString(obj, Address.Keys.ADDR1));
        address.setCity(JSONHelper.getString(obj, Address.Keys.CITY));
        address.setState(JSONHelper.getString(obj, Address.Keys.STATE));
        address.setZip(JSONHelper.getString(obj, Address.Keys.ZIP));
        address.setPhone(JSONHelper.getString(obj, Address.Keys.PHONE));
        address.setLat(JSONHelper.getDouble(obj, Address.Keys.LAT));
        address.setLng(JSONHelper.getDouble(obj, Address.Keys.LNG));
        address.set__v(JSONHelper.getInt(obj, Address.Keys.V));
        return address;
    }

    public static String getDisplayText(Address address) {
        return String.format("%s %s %s %s",
            address.getAddr1(),
            address.getCity(),
            address.getState(),
            address.getZip());
    }
}
