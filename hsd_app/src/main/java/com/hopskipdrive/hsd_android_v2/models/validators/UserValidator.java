package com.hopskipdrive.hsd_android_v2.models.validators;

import com.hopskipdrive.hsd_android_v2.models.User;

public class UserValidator {
    public static boolean isValid(User user) {
        return
                user != null &&
                user.getLoginData() != null;
    }

    public static boolean isDriver(User user) {
        return
            user != null &&
            user.getLoginData() != null &&
            user.getLoginData().getIsDriver();
    }

    public static boolean isParent(User user) {
        return
            user != null &&
                user.getLoginData() != null &&
                user.getLoginData().getIsParent();
    }
}
