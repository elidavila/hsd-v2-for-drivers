package com.hopskipdrive.hsd_android_v2.models.translators;

import com.crashlytics.android.Crashlytics;
import com.hopskipdrive.hsd_android_v2.models.Address;
import com.hopskipdrive.hsd_android_v2.models.Driver;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.Rider;
import com.hopskipdrive.hsd_android_v2.models.validators.AddressValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.DriverValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RiderValidator;
import com.hopskipdrive.hsd_android_v2.util.JSONHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RidesTranslator {
    public static List<Ride> getRidesList(String json) {
        List<Ride> rides = new ArrayList<>();
        try {
            JSONArray ridesArray = new JSONArray(json);
            for(int i = 0; i < ridesArray.length(); i++) {
                JSONObject obj = ridesArray.getJSONObject(i);
                Ride ride = getRide(obj);
                if (RideValidator.isValid(ride)) {
                    rides.add(ride);
                }
            }
        }
        catch (Exception e) {
            Crashlytics.logException(e);
        }
        return rides;
    }

    public static Ride getRide(JSONObject obj) {
        Ride ride = new Ride();
        ride.set_id(JSONHelper.getString(obj, Ride.Keys.ID));
        ride.setDriver_fee(JSONHelper.getInt(obj, Ride.Keys.DRIVER_FEE));
        ride.setActual_time(JSONHelper.getString(obj, Ride.Keys.ACTUAL_TIME));
        ride.setActual_distance(JSONHelper.getDouble(obj, Ride.Keys.ACTUAL_DISTANCE));
        ride.setDriving_summary(JSONHelper.getString(obj, Ride.Keys.DRIVING_SUMMARY));
        ride.setDriving_duration(JSONHelper.getString(obj, Ride.Keys.DRIVING_DURATION));
        ride.setDriving_distance(JSONHelper.getString(obj, Ride.Keys.DRIVING_DISTANCE));
        ride.setDropoff_ts(JSONHelper.getLong(obj, Ride.Keys.DROPOFF_TS));
        ride.setPickup_ts(JSONHelper.getLong(obj, Ride.Keys.PICKUP_TS));
        ride.setStatus(JSONHelper.getString(obj, Ride.Keys.STATUS));
        ride.setParent(JSONHelper.getString(obj, Ride.Keys.PARENT));
        ride.setName(JSONHelper.getString(obj, Ride.Keys.NAME));
        ride.setDate(JSONHelper.getString(obj, Ride.Keys.DATE));
        ride.setPickup(JSONHelper.getString(obj, Ride.Keys.PICKUP));
        ride.setEscort1(JSONHelper.getBoolean(obj, Ride.Keys.ESCORT1));
        ride.setEscort2(JSONHelper.getBoolean(obj, Ride.Keys.ESCORT2));
        ride.setDropoff(JSONHelper.getString(obj, Ride.Keys.DROPOFF));
        ride.setNotes(JSONHelper.getString(obj, Ride.Keys.NOTES));
        ride.set__v(JSONHelper.getInt(obj, Ride.Keys.V));
        ride.setDropOffNotes(JSONHelper.getString(obj, Ride.Keys.DROPOFF_NOTES));
        ride.setPickUpNotes(JSONHelper.getString(obj, Ride.Keys.PICKUP_NOTES));
        ride.setShiftId(JSONHelper.getString(obj, Ride.Keys.SHIFT_ID));
        ride.setCheckedIn(JSONHelper.getBoolean(obj, Ride.Keys.CHECKED_IN));
        ride.setActual_pickup_ts(JSONHelper.getLong(obj, Ride.Keys.ACTUAL_PICKUP_TS));
        ride.setActual_dropoff_ts(JSONHelper.getLong(obj, Ride.Keys.ACTUAL_DROPOFF_TS));

        JSONObject startingAddressObj = JSONHelper.getJSONObject(obj, Ride.Keys.STARTING_ADDRESS);
        Address startingAddress = AddressTranslator.getAddress(startingAddressObj);
        if (AddressValidator.isValid(startingAddress)) {
            ride.setSaddr(startingAddress);
        }

        JSONObject destinationAddressObj = JSONHelper.getJSONObject(obj, Ride.Keys.DESTINATION_ADDRESS);
        Address destinationAddress = AddressTranslator.getAddress(destinationAddressObj);
        if (AddressValidator.isValid(startingAddress)) {
            ride.setDaddr(destinationAddress);
        }

        JSONArray ridersObj = JSONHelper.getJSONArray(obj, Ride.Keys.RIDERS);
        List<Rider> riders = RiderTranslator.getRiders(ridersObj);
        if (RiderValidator.isValid(riders)) {
            ride.setRiders(riders);
        }

        JSONObject driverObj = JSONHelper.getJSONObject(obj, Ride.Keys.DRIVER);
        Driver driver = DriverTranslator.getDriver(driverObj);
        if (DriverValidator.isValid(driver)) {
            ride.setDriver(driver);
        }

        //Set the phone numbers --> map(k,v) = (name, number)
        JSONObject phoneNumbersObj = JSONHelper.getJSONObject(obj, Ride.Keys.PHONE_NUMBERS);
        if(phoneNumbersObj != null) {
            LinkedHashMap<String, String> phoneNumbersMap = PhoneNumbersTranslator.getPhoneNumbers(phoneNumbersObj, ride.getParent(), riders);
            if (phoneNumbersMap != null && phoneNumbersMap.size() > 0)
                ride.setPhone_numbers(phoneNumbersMap);
        }

        return ride;
    }

    public static JSONObject getRideStatusParams(Ride.Status status) {
        JSONObject obj = new JSONObject();
        JSONHelper.put(obj, Ride.Keys.STATUS, status.getName());
        JSONHelper.put(obj, "timestamp", System.currentTimeMillis());
        return obj;
    }

    public static JSONObject getDeviceParams(String device, String token){
        JSONObject obj = new JSONObject();
        JSONHelper.put(obj, device, token);
        return obj;
    }
}
