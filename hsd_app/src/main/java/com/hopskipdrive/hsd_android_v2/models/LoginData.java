package com.hopskipdrive.hsd_android_v2.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Eli Davila on 5/11/2015.
 * File that contains getters and setters from the login response
 */
public class LoginData implements Serializable {
    boolean ok = false;
    private String _id, firstName, errorMsg;
    private boolean isDriver, isParent, pushPrefs, smsPrefs, emailPrefs;

    public void setOk(boolean ok){
        this.ok = ok;
    }
    public boolean getOk(){
        return ok;
    }

    public void set_id(String _id){
        this._id = _id;
    }
    public String get_id(){
        return _id;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public String getFirstName(){
        return firstName;
    }

    public void setIsParent(boolean isParent){
        this.isParent = isParent;
    }
    public boolean getIsParent(){
        return isParent;
    }

    public void setIsDriver(boolean isDriver){
        this.isDriver = isDriver;
    }
    public boolean getIsDriver(){
        return isDriver;
    }

    public void setPushPrefs(boolean pushPrefs){
        this.pushPrefs = pushPrefs;
    }
    public boolean getPushPrefs(){
        return pushPrefs;
    }

    public void setSmsPrefs(boolean smsPrefs){
        this.smsPrefs = smsPrefs;
    }
    public boolean getSmsPrefs(){
        return smsPrefs;
    }

    public void setEmailPrefs(boolean emailPrefs){
        this.emailPrefs = emailPrefs;
    }
    public boolean getEmailPrefs(){
        return emailPrefs;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    public String getErrorMsg(){
        return errorMsg;
    }

    public static class Keys {
        public static final String DRIVER = "driver";
        public static final String PARENT = "parent";
        public static final String FIRST = "first";
        public static final String OK = "ok";
        public static final String ERR = "err";
    }
}
