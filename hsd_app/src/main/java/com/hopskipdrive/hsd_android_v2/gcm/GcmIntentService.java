package com.hopskipdrive.hsd_android_v2.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.activities.DriveDetailsActivity;
import com.hopskipdrive.hsd_android_v2.activities.RidesActivity;
import com.hopskipdrive.hsd_android_v2.activities.SplashActivity;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.models.User;
import com.hopskipdrive.hsd_android_v2.models.validators.UserValidator;

import javax.inject.Inject;

public class GcmIntentService extends IntentService {
    private static final String INTENT_KEY_RIDE_ID = "Ride.Id";
    @Inject HSDSession mSession;
    User user;

    public GcmIntentService() {
        super("GcmIntentService");
        HSDInject.inject(this);
    }

    @Override
    public void onCreate() { super.onCreate(); }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
            sendNotification(extras);
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(Bundle extras) {
        String message = extras.getString("key1");
        String title = extras.getString("collapse_key");
        String rideId = extras.getString("rideId");
        String from = extras.getString("from");

        if(!from.equals("google.com/iid")) {
            int requestID = (int) System.currentTimeMillis();
            NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent pendingIntent;
            user = mSession.getUser();

            if(UserValidator.isDriver(user)){
                Intent intent = new Intent(this, DriveDetailsActivity.class);
                intent.putExtra(INTENT_KEY_RIDE_ID, rideId);
                pendingIntent = PendingIntent.getActivity(this, requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }else if(UserValidator.isParent(user)) {
                //TODO: Parent side
                pendingIntent = PendingIntent.getActivity(this, requestID, new Intent(this, RidesActivity.class), 0);
            }else{
                pendingIntent = PendingIntent.getActivity(this, requestID, new Intent(this, SplashActivity.class), 0);
            }

            //Create the push notification with data
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.app_icon)
                            .setContentTitle(title)
                            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                            .setAutoCancel(true)
                            .setLights(R.color.orange_color, 1000, 1000)
                            .setOnlyAlertOnce(true)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                            .setContentText(message);
            mBuilder.setContentIntent(pendingIntent);
            mNotificationManager.notify(requestID, mBuilder.build());
        }
    }
}
