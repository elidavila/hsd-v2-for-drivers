package com.hopskipdrive.hsd_android_v2.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.fragments.RidesListFragment;

import javax.inject.Inject;

public class RidesPagerAdapter extends FragmentStatePagerAdapter {

    public enum RidesListType {
        UPCOMING (R.string.upcoming_string),
        COMPLETED (R.string.completed_string);

        private final int mTitleResId;

        RidesListType(int titleResId) {
            mTitleResId = titleResId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }
    }

    @Inject Context mContext;

    public RidesPagerAdapter(FragmentManager fm) {
        super(fm);
        HSDInject.inject(this);
    }

    @Override
    public Fragment getItem(int position) {
        return RidesListFragment.newInstance(RidesListType.values()[position]);
    }

    @Override
    public int getCount() {
        return RidesListType.values().length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(RidesListType.values()[position].getTitleResId());
    }
}
