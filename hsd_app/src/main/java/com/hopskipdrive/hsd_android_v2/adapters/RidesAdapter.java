package com.hopskipdrive.hsd_android_v2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.hopskipdrive.hsd_android_v2.R;
import com.hopskipdrive.hsd_android_v2.app.HSDInject;
import com.hopskipdrive.hsd_android_v2.app.HSDSession;
import com.hopskipdrive.hsd_android_v2.models.Ride;
import com.hopskipdrive.hsd_android_v2.models.User;
import com.hopskipdrive.hsd_android_v2.models.validators.DriverValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.RideValidator;
import com.hopskipdrive.hsd_android_v2.models.validators.UserValidator;
import com.hopskipdrive.hsd_android_v2.util.HelperFunctions;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

public class RidesAdapter extends RecyclerView.Adapter<RidesAdapter.RidesViewHolder> {

    private List<Ride> mRides = Collections.emptyList();
    private String[] driveTimes;
    final OnRideClickedListener mListener;

    @Inject Context mContext;
    @Inject HSDSession mSession;

    public RidesAdapter(OnRideClickedListener listener) {
        HSDInject.inject(this);
        mListener = listener;
    }

    @Override
    public RidesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_upcoming_ride, parent, false);
        RidesViewHolder holder = new RidesViewHolder(view, mListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RidesViewHolder holder, int position) {
        Ride ride = mRides.get(position);
        if (RideValidator.isValid(ride)) {
            driveTimes = HelperFunctions.getRegularTime(ride.getPickUpDate().toString());
            holder.bind(mContext, ride, mSession.getUser(), driveTimes);
        }
    }

    @Override
    public int getItemCount() {
        return mRides.size();
    }

    public void bind(List<Ride> rides) {
        mRides = rides;
        notifyDataSetChanged();
    }

    static class RidesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final OnRideClickedListener mListener;
        Ride mRide;

        @InjectView(R.id.name) TextView mName;
        @InjectView(R.id.date) TextView mDate;
        @InjectView(R.id.driver) ImageView mDriver;
        @InjectView(R.id.rideBorderLine) View mBorderLine;

        public RidesViewHolder(View itemView, OnRideClickedListener listener) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            if (mListener != null && RideValidator.isValid(mRide)) {
                mListener.onRideClicked(mRide);
            }
        }

        public void bind(Context context, Ride ride, User user, String[] driveTimes) {
            //Set image icon
            if(UserValidator.isParent(user) && DriverValidator.hasPhoto(ride.getDriver()) && !UserValidator.isDriver(user))
                Picasso.with(context).load(ride.getDriver().getPhoto_url()).into(mDriver);
            else
                Picasso.with(context).load(R.mipmap.today_rides3x).into(mDriver);

            mRide = ride;

            //Set date of ride
            if(driveTimes != null && driveTimes.length >= 4)
                mDate.setText(driveTimes[0] + " " + driveTimes[1] + " " + driveTimes[2] + " at " + driveTimes[3]);
            else
                mDate.setText(ride.getDate());

            //Set ride name
            mName.setText(ride.getName());

            //Set divider line
            mBorderLine.setVisibility(View.VISIBLE);
        }
    }

    public interface OnRideClickedListener {
        void onRideClicked(Ride ride);
    }
}